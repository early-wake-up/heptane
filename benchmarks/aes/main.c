// Enable both ECB and CBC mode. Note this can be done before including aes.h or at compile-time.
// E.g. with GCC by using the -D flag: gcc -c aes.c -DCBC=0 -DECB=1
#define CBC 1
#define ECB 1

#include "msp430.h"
#include "uart.h"
#include "platform.h"
#include "mementos.h"
#include "adc.h"
#include "aes.h"

#define TIMER_INIT 0xFF40
#define START_EXPERIMENT BIT5
#define MEASURE BIT4
#define EXPERIMENT_RUNNING BIT7

static void phex(uint8_t *str);
static void test_encrypt_ecb(void);
static void test_decrypt_ecb(void);
static void test_encrypt_ecb_verbose(void);
static void test_encrypt_cbc(void);
static void test_decrypt_cbc(void);

#define printf(STR) ;

void sleep_until_voltage_reached(int value)
{
  // Early return if voltage is already reached
  int voltage = adc_measure();
  if (voltage > value)
  {
    return;
  }
  // If voltage is not reached, setup an interrupt and enter LPM3
  CSCTL0_H = CSKEY >> 8; // Unlock CS registers
  // CSCTL2 |= SELA__LFXTCLK | SELS__DCOCLK | SELM__DCOCLK; // set ACLK = XT1; MCLK = DCO
  CSCTL2 |= SELA__LFXTCLK;
  CSCTL3 |= DIVA__1; // Set all dividers
  CSCTL4 &= ~LFXTOFF;
  //   do
  //   {
  //     CSCTL5 &= ~LFXTOFFG;                    // Clear XT1 fault flag
  //     SFRIFG1 &= ~OFIFG;
  //   } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag
  CSCTL0_H = 0; // Lock CS registers

  TA0CTL = TASSEL__ACLK | MC__CONTINUOUS | TACLR | TAIE; // ACLK, contmode, clear TAR
  TA0R = TIMER_INIT;
  __bis_SR_register(LPM3_bits | GIE); // Enter LPM3, enable interrupts

  while (1)
  {
    P1OUT |= MEASURE;
    int voltage = adc_measure();
    P1OUT &= ~MEASURE;
    if (voltage > value)
    {
      break;
    }
    else
    {
      TA0R = TIMER_INIT;
      __bis_SR_register(LPM3_bits | GIE); // Enter LPM3, enable interrupts
    }
  }
  // uart_puts("Stopping LFXT...");
  CSCTL0_H = CSKEY >> 8;
  CSCTL4 |= LFXTOFF;
  // uart_puts("[DONE]\nModifying TA0CTL\n");
  TA0CTL = MC__STOP | TACLR;
  // uart_puts("[DONE]\n");
}

// BUTTON VERSION
void wait_for_button_press()
{
    // Set P1.1 as INPUT
    P1DIR &= ~BIT1;
    P1OUT |= BIT1;
    P1REN |= BIT1;
    // uart_puts("Ready to execute, press button to start\n");
    // Generate interrupt on a low to high transition
    P1IV |= BIT1;
    P1IES |= BIT1;
    P1IE |= BIT1;
    P1IFG = 0;
    __bis_SR_register(LPM3_bits | GIE);
    P1OUT |= 0;
    P1IES = 0;
    P1IFG = 0;
}

void wait_for_input()
{
  // Set P1.5 as INPUT
  P1DIR &= ~START_EXPERIMENT;
  P1OUT &= ~START_EXPERIMENT;
  // uart_puts("Ready to execute, press button to start\n");
  // Generate interrupt on a low to high transition
  P1IV |= START_EXPERIMENT;
  P1IES &= ~START_EXPERIMENT;
  P1IE |= START_EXPERIMENT;
  P1IFG = 0;
  __bis_SR_register(LPM3_bits | GIE);
  P1OUT |= 0;
  P1IES = 0;
  P1IFG = 0;
}

int main(void)
{
  platform_init();
  adc_init();
  P1DIR |= EXPERIMENT_RUNNING;
  // uart_setup();
  // printf("Hello wolrd\n");
  sleep_until_voltage_reached(2345);

  int res = mementos_boot();
  if (res)
  {
    printf("Mementos boot failed\n");
    return 1;
  }

  P1OUT |= EXPERIMENT_RUNNING;
  // printf("Starting experiment\n");
  // uart_setup();
  // printf("Hello World!\n");
  while (1)
  {
    printf("Waiting for button press\n");
    wait_for_input();
    printf("Button pressed !\n");
    P1OUT &= ~EXPERIMENT_RUNNING;

    load();

    P1OUT |= EXPERIMENT_RUNNING;
  }

  //

  return 0;
}

// Timer0_A1 Interrupt Vector (TAIV) handler
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER0_A1_VECTOR
__interrupt void TIMER0_A1_ISR(void)
#elif defined(__GNUC__)
void __attribute__((interrupt(TIMER0_A1_VECTOR))) TIMER0_A1_ISR(void)
#else
#error Compiler not supported!
#endif
{
    switch (__even_in_range(TA0IV, TA0IV_TAIFG))
    {
    case TA0IV_NONE:
        break; // No interrupt
    case TA0IV_TACCR1:
        break; // CCR1 not used
    case TA0IV_TACCR2:
        break; // CCR2 not used
    case TA0IV_3:
        break; // reserved
    case TA0IV_4:
        break; // reserved
    case TA0IV_5:
        break; // reserved
    case TA0IV_6:
        break;        // reserved
    case TA0IV_TAIFG: // overflow
        // P1OUT ^= BIT0;
        __bic_SR_register_on_exit(LPM3_bits);
        // uart_puts("Returning from LPM3 !\n");
        break;
    default:
        break;
    }
}

// GPIO interrupt PORT1
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = PORT1_VECTOR
_interrupt void PORT1_ISR(void)
#elif defined(__GNUC__)
void __attribute__((interrupt(PORT1_VECTOR))) PORT1_ISR(void)
#else
#error Compiler not supported!
#endif
{
    P1IFG = 0;
    __bic_SR_register_on_exit(LPM3_bits | GIE);
}
