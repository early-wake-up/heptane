#include <annot.h>
void checkpoint() {}

void bar() {}

void foo(int i) {
    if(i == 0) {
        bar();
    } else {
        checkpoint();
    }
}

void main() {
    bar();
    for(int i = 0; i< 2; i++) {
        ANNOT_MAXITER(2);
        foo(i);
    }
}
