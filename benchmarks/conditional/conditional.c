// BB0: 2 cycles
void checkpoint() { }

void main() {

  // BB1 : 8 cycles
  int i,j,k;

  j = j + k -1;

  if(j == 2){
    // BB2 : 2 cycles
    i = 0;
    checkpoint();
    // BB3 : 1 cycle (only jump)
  } else {
    // BB4 : 1 cycle
    i = 1;
  }

  // BB5 : 5 cycle
  k ++;
  j--;
}
