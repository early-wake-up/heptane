//  2 cycles
void checkpoint() {}

void foo(int j)
{
    // BB1 : 5 cycles
    if (j > 0)
    {
        // BB2 : 1 cycles + call to checkpoint
        checkpoint();
        // BB3 : 2 cycle (incr + jump)
        j--;
    }
    else
    {
        // BB4 : 1 cycle
        j++;
    }
    // BB5 : 3 cycle
}

void bar(int j)
{
    foo(j);
}

int main()
{
    // BB6 : 8 cycles
    int i, j, k;

    foo(j);
    bar(j);
    j = j + k - 1;

    foo(j);

    // BB7 : 3 cycle
    return 0;
}
