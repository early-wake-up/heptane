// BB0 2 cycles
void checkpoint() { }

// Nb cycles:
// - BB1 8 + call checkpoint: 10
// - BB2 1 (jmp)
// - 10 * loop: 63
//  - Header : 3
//  - Body : 3
//  - Header final: 3
// - BB2 3 (nop, add, ret)
void main() {
  // BB1 8 + 4 (call ckpt) cycles
  int i,j,k;
  j = j + k -1;
  i = 0;
  checkpoint();
  // Jump BB : 1 Cycle
  // Header: 3 Cycles
  for(; i<10; i++) {
    // Body: 3 cycles
    j = j + i;
    k++;
  }

  // BB2 3 cycles (nop, add, ret)
  return;
}
