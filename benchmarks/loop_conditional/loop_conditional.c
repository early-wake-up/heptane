// BB0 2 cycles
void checkpoint() { }

// Nb cycles:
// - BB1 8
// - 10 * loop: 93
//  - BB5 Header: 3
//  - BB2 4
//  - BB3 : 3 (Call chkpt: 1 + 2)
//  - BB4 : 2
// - BB6 3 (nop, add, ret)
void main() {
  // BB1 8 cycles
  int i,j,k;
  j = j + k -1;
  i = 0;
  // BB5 Header: 3 cycles
  for(; i<10; i++) {
    // BB2 4 cycles
    j = j + i;
    if(i > 3) {
      // BB3 3 cycles
      checkpoint();
    }
    // BB4 footer: 2 cycles (k++, i++)
    k++;
  }

  // BB6 3 cycles (nop, add, ret)
  return;
}
