#!/bin/sh

testfailure="false"

export HEPTANE_FORCE_MODE=1

# Look devices.csv file to get infos about the ISA  (before option -mcpu=xxx was used, now deprecated).
# for ISA 430 : ARCH=MSP430F4250, for ISA 430X: ARCH= , for ISA 430xv2 : ARCH=MSP430FR5739
ARCH=MSP430FR5969

SOLVER=cplex
DIR_SCRIPT=./benchmarks

if [ ! -e ${DIR_SCRIPT}/../bin/HeptaneExtract ]; then
   echo "HeptaneExtract not found. Exiting..."
   exit -1
fi

if [ ! -e ${DIR_SCRIPT}/../bin/HeptaneAnalysis ]; then
   echo "HeptaneExtract not found. Exiting..."
   exit -1
fi

vhour=$(date +%H%M)
vday=$(date +%Y%m%d)

TEST_SET="simple conditional loop function function_double bs loop loop_checkpoint_bb1 loop_conditional complete nested_loops"
# simple conditional loop function"
${DIR_SCRIPT}/run_benchmarks.sh "$TEST_SET" $ARCH $SOLVER  | tee LOGFILE_${ARCH}_${vday}_${vhour}
