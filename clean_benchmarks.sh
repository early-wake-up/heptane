BENCHMARK_DIR="benchmarks"

rm -f LOGFILE_*
for BENCHMARK in $(ls $BENCHMARK_DIR); do
    if [ -d "$BENCHMARK_DIR/$BENCHMARK" ]; then
        cd $BENCHMARK_DIR/$BENCHMARK
        echo "Cleaning $BENCHMARK"
        rm -rf *.xml *.log *.txt *.s *.pdf 1 *.dot *.exe *.objdump *.readelf *.html *. project
        rm -rf checkpoint_analysis checkpoint_energy.csv
        cd ../..
    fi
done
