import numpy as np
import pandas as pd

ml_models = ['Time', 'Energy',  #  Observed
             'QLRregressor_WCET_worst', 'QLRRregressor_WCEC_worst',  # QLR based
             'GradBoostRregressor_WCET_worst', 'GradBoostRegressor_WCEC_worst',  # GB based
             #  'SVRRregressor_WCET_worst', 'SVRRregressor_WCEC_worst',  # SVR based
             # 'RFRregressor_WCET_worst', 'RFRregressor_WCEC_worst',  # RF based
             'MLPregressor_WCET_worst', 'MLPRregressor_WCEC_worst',  # NN based
             ]

models_class = {
    'QLRregressor_WCET_worst': 'QLR',
    'GradBoostRregressor_WCET_worst': 'GB',
    'MLPregressor_WCET_worst': 'MLP',
}

energy_observed = {
    "bs": 358,
    "fibcall": 1059,
    "insertsort": 3453,
    "lcdnum": 1019,
    "nsichneu": 25179,
}

time_observed = {
    "bs": 293,
    "fibcall": 1035,
    "insertsort": 3309,
    "lcdnum": 919,
    "nsichneu": 24672,
}

print(len(ml_models), "models")
print(ml_models)

def pairwise(iterable):
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    a = iter(iterable)
    return zip(a, a)

def generate_overestimation_csv(csv_file):
    # Read the CSV into a DataFrame
    df = pd.read_csv(csv_file)
    # Use pivot_table to create a dictionary of {benchmark: {model: value}}
    data_dict = df.pivot_table(
        index='Model', columns='Benchmark', values='Value').to_dict()

    # Get the list of benchmarks and the list of models
    benchmarks = sorted(df['Benchmark'].unique())
    models = sorted(df['Model'].unique())
    models.append('Time')
    models.append('Energy')

    out_csv = "overestimation.csv"
    with open(out_csv, 'w') as f:
        header = ["Benchmark"]
        for model in models_class.values():
            header.append(model)
        f.write(",".join(header) + "\n")
        for benchmark in benchmarks:
            row = [benchmark]
            benchmark_data = data_dict[benchmark]
            i = 0
            ground_truth_time = time_observed[benchmark]
            # Loop through each model and retrieve the corresponding value if available
            for model in models_class:
                if model not in models:
                    print(f"Model {model} not found in the CSV file")
                    value = -1
                else:
                    value = benchmark_data.get(model, '')
                value = 100 * value / ground_truth_time - 100
                row.append(f"{value:.2f}")
            f.write(",".join(row) + "\n")


def generate_latex_table(csv_file):

    # Read the CSV into a DataFrame
    df = pd.read_csv(csv_file)
    # Use pivot_table to create a dictionary of {benchmark: {model: value}}
    data_dict = df.pivot_table(
        index='Model', columns='Benchmark', values='Value').to_dict()

    # Get the list of benchmarks and the list of models
    benchmarks = sorted(df['Benchmark'].unique())
    models = sorted(df['Model'].unique())
    models.append('Time')
    models.append('Energy')
    # Prepare the LaTeX table header
    latex_table = """%                   Maximum observed                           QLR based       GB based        SVR based       RF based        NN based
%               Time    Energy             TPRD    EPRD    TPRD    EPRD    TPRD    EPRD    TPRD    EPRD    TPRD    EPRD"""

    overestimation_max = []
    # Loop through each benchmark and generate the LaTeX table rows
    for benchmark in benchmarks:
        latex_table += f"\n{benchmark} "
        data_dict[benchmark]['Time'] = time_observed[benchmark]
        data_dict[benchmark]['Energy'] = energy_observed[benchmark]
        benchmark_data = data_dict[benchmark]
        i = 0


        # Loop through each model and retrieve the corresponding value if available
        for model in ml_models:
            if model not in models:
                print(f"Model {model} not found in the CSV file")
                value = -1
            else:
                value = benchmark_data.get(model, '')
            if i % 2 == 0: # Time
                latex_table += f"\n& \\multicolumn{{1}}{{r|}}{{\\n{{{value}}}}} "
            else: # Energy
                latex_table += f"& \\n{{{value}}} "
            i += 1
        # Add the closing "\\" and the horizontal line
        latex_table += "\\\\ \hline"

    # multicol_start="\\multicolumn{1}{r|}{"
    # multicol_end="\\%}"
    # # Add the overestimation to the LaTeX table
    # latex_table += "\n \\multicolumn{2}{|r|}{\\multirow{2}{*}{Overestimation}} & \\multicolumn{1}{r|}{ mean}"
    # for over_0, over_1  in pairwise(overestimation_max):
    #     latex_table += "\n&" + multicol_start + f"{over_0:.0f}" + multicol_end +  f"& {over_1:.0f}\\%"
    # # Add the closing "\\" and the horizontal line
    # latex_table += "\\\\ \\cline{3-9}"

    # latex_table += "\n \\multicolumn{2}{|r|}{} & \\multicolumn{1}{r|}{min}"
    # for over_0, over_1  in pairwise(overestimation_min):
    #     latex_table += "\n&" + multicol_start + f"{over_0:.0f}" + multicol_end +  f"& {over_1:.0f}\\%"

    # Add the closing "\\" and the horizontal line
    latex_table += "\\\\ \\hline"

    return latex_table


# Specify the input CSV file path
csv_file_path = 'ml_results.csv'

# Generate the LaTeX table rows
latex_table_rows = generate_latex_table(csv_file_path)

# Generate a CSV file with overestimation per benchmark/model
generate_overestimation_csv(csv_file_path)

# Print the LaTeX table rows
print(latex_table_rows)
