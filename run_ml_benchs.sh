#!/bin/bash
RED="\e[31m"
CYAN="\e[36m"
GRAY="\e[90m"
ENDCOLOR="\e[0m"
# Define the CSV file name
MLWCET_PATH=/home/hreymond/code/ML_NASSIM_WCEC/mlwcetheptaneplugin/
model_folder=$MLWCET_PATH/models
csv_file="ml_results.csv"
echo "Model,Benchmark,Value" > $csv_file

rm -f LOGFILE* ml_logs.log
# Function to run the benchmarks for a given model
# $1: Model name
function run_benchmark_with_model() {
    model=$1
    ML_WCET=1 ML_MODEL=$model bash -c "./benchmarks/mktests_MSP430.sh" > ml_logs.log
    error=$(cat ml_logs.log | grep -i "error")
    if [[ -n "$error" ]]; then
        echo -e "$RED $error $ENDCOLOR"
    fi
    result=$(cat ml_logs.log | grep "benchmark\|WCET:")

    # Define variables to store the benchmark and WCET values
    benchmark=""
    wcet=""

    # Read the input lines
    while read -r line; do
        # Check if the line contains "benchmark" and extract the value
        if [[ $line == *"benchmark:"* ]]; then
            benchmark=$(echo "$line" | awk -F ':  ' '{print $2}')
        fi

        # Check if the line contains "WCET" and extract the value
        if [[ $line == *"WCET"* ]]; then
            wcet=$(echo "$line" | awk -F ': ' '{print $2}')
        fi

        # Check if both benchmark and WCET values are non-empty
        if [[ -n "$benchmark" && -n "$wcet" ]]; then
            # Get the model name from the file name (replace the extension with an empty string)
            model_name=$(basename "$model" | sed 's/\.[^.]*$//')
            # Output the benchmark and WCET values
            echo -e "$GRAY Benchmark: $benchmark, WCET: $wcet $ENDCOLOR"
            # Append the benchmark and WCET values to the CSV file
            echo "$model_name,$benchmark,$wcet" >>"$csv_file"
            # Reset the variables for the next iteration
            benchmark=""
            wcet=""
        fi
    done <<<"$result"
    # rm ml_logs.log
}

model_found=false
# Run the benchmarks for the all models
for model_path in "$model_folder"/*; do
    # Check if the file is a regular file (not a directory)
    if [[ -f "$model_path" ]]; then
        echo -e "$CYAN Running benchmarks for model: $model_path $ENDCOLOR"
        run_benchmark_with_model "$model_path"
        model_found=true
    fi
done

if [[ "$model_found" = false ]]; then
    echo -e "$RED No model found in $model_folder $ENDCOLOR"
fi
