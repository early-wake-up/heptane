#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
echoerr() { echo -e "$RED$@$NC" 1>&2; }
echook() { echo -e "$GREEN$@$NC"; }

LOG_FILE=/tmp/log_heptane
RESULT_FILE=checkpoint_energy.csv
EXPECTED_FILE=checkpoint_energy_expected.csv
# Delete previous results
find . -name "$RESULT_FILE" -delete

# Step 1: Build the tool

cd src
echo "Cleaning installation..."
make clean > /dev/null
echo "Building the tool..."
if ! make > /dev/null; then
    echoerr "Error while building the tool"
    exit 1
fi
cd ..

# Remove old "checkpoint_energy.csv" files
find . -name "$RESULT_FILE" -delete

# Step 2 Run benchmarks
cp config_files/configWCET_template_MSP430.xml config_files/configWCET_template_MSP430.xml.old2
cp config_files/configMSP430_benchmarks.xml config_files/configWCET_template_MSP430.xml
echo "Running benchmarks..."

source ./benchmarks/mktests_MSP430_benchmarks.sh >${LOG_FILE}
if [ $? -ne 0 ]; then
    echoerr "Error while running benchmarks"
    exit 1
fi

cp config_files/configWCET_template_MSP430.xml.old2 config_files/configWCET_template_MSP430.xml
rm config_files/configWCET_template_MSP430.xml.old2

# Step 3: Check for $RESULT_FILE in each folder of the benchmark directory
echo "Checking results..."

for benchmark in $TEST_SET; do
    item="benchmarks/$benchmark"
    if ! [ -d "$item" ]; then
        continue
    fi
    folder="$item"
    if ! [ -f "$folder/$RESULT_FILE" ]; then
        echoerr "$RESULT_FILE not found in $folder"
        exit 1
    fi
    if ! [ -f "$folder/$EXPECTED_FILE" ]; then
        echoerr "$EXPECTED_FILE not found in $folder"
        exit 1
    fi
    diff -q $folder/$RESULT_FILE $folder/$EXPECTED_FILE
    return="$?"
    # Check the return code
    if [ $return -eq 0 ]; then
        echook "$folder [OK]"
    elif [ $return -eq 1 ]; then
        echoerr "$folder/$RESULT_FILE differs from the expected csv !"
        echoerr "$folder [NOT OK]"
    elif [ $return -eq 2 ]; then
        echoerr "Error while comparing $folder/$RESULT_FILE and $folder/$EXPECTED_FILE"
        echoerr "$folder [NOT OK]"
    else
        echoerr "Unexpected return code"
        exit 1
    fi
done
