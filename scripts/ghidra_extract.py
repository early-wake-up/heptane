"""
Script designed to run inside Ghidra, with the Ghidrathon plugin

This script extracts the CFGs from a program loaded in Ghidra and converts them to the Heptane format

See [Ghidrathon Github](https://github.com/mandiant/Ghidrathon)

To run the script:
Create a brand new project, with the file you wish to analyze in it
```sh
mkdir project
/opt/ghidra/support/analyzeHeadless ./project project.gpr -import <elf_file> -processor <Processor ID>
```
Then, process your file and run your python script :

```sh
/opt/ghidra/support/analyzeHeadless ./project project.gpr -process <elf_file> -postScript extract_program.py
```

"""
import logging
import os
import re
import xml.etree.ElementTree as ET
from natsort import natsorted
from ghidra.program.model.block import SimpleBlockIterator, SimpleBlockModel
from ghidra.program.model.address import Address

LIBC_FUNCTIONS_ITER_ANNOT = {
    "mpyi": 16,
}

IGNORED_FUNCTIONS = ['register_tm_clones', 'deregister_tm_clones', '__do_global_dtors_aux', '__do_global_ctors_aux', 'call___do_global_ctors_aux',
                     '_msp430_run_array', 'frame_dummy', '_msp430_run_fini_array', '_msp430_run_init_array', '_msp430_run_preinit_array', '__crt0_call_exit', '__crt0_call_init_then_main']

global_id = 0

def get_id():
    # Get the global ID and increment it
    global global_id
    global_id += 1
    return str(global_id)

def opcode_in_list(inst, opcode_list):
    for opcode in opcode_list:
        if inst.startswith(opcode):
            return True
    return False

# Instruction that use immediate values in decimal format
immediates = ["SUB", "ADD", "AND", "XOR", "RPT", "CMP"]

def msp430_asm_to_heptane_fmt(asm):
    """
    Convert the assembly instruction from the ghidra format to the Heptane format
    """
    # Replace spaces by tabs
    '\t'.join(asm.split())
    # Change the register names
    asm = asm.replace("SP", "R1").replace("PC", "R0").replace("SR", "R2")
    # Remove the .w suffix (as it is the default if no suffix is specified)
    asm = asm.replace(".W", "")

    asm = asm.replace("BRA", "BR")
    # replace #0xN by #N
    if not asm.startswith("CALL") and not asm.startswith("BR"):
        result = re.search(r"#0x([0-9a-fA-F]+)", asm)
        while result is not None:
            imm = int(result.group(1), base=16)
            asm = asm.replace(result.group(0), f"{imm}")
            result = re.search(r"#0x([0-9a-fA-F]+)", asm)
    # Remove & and # as HeptaneExtract consider them as "useless" characters
    asm = asm.replace("&", "").replace("#", "")
    # Replace the { in rpt instructions by ,
    asm = asm.replace("{", ",")
    # Handle POPM and PUSHM instructions
    result = re.search(r"PUSHM 0x([0-9a-fA-F]+)", asm)
    if result is not None:
        imm = int(result.group(1), base=16)
        asm = asm.replace(result.group(0), f"PUSHM {imm}")
    result = re.search(r"POPM 0x([0-9a-fA-F]+)", asm)
    if result is not None:
        imm = int(result.group(1), base=16)
        asm = asm.replace(result.group(0), f"POPM {imm}")
    # Replace 0xN(Rx) by N(Rx), and translate N to decimal
    result = re.search(r"0x([0-9a-fA-F]+)\((.+)\)", asm)
    while result is not None:
        imm = int(result.group(1), base=16)
        reg = result.group(2)
        asm = asm.replace(result.group(0), f"{imm}({reg})")
        result = re.search(r"0x([0-9a-fA-F]+)\((.+)\)", asm)
    # asm = asm.replace("#", "")
    return asm.lower()


def get_symbols_at(addr):
    """
    Get all the symbols at a given address

    Used to desambiguate between the elf symbol table and the symbol table
    generated by Ghidra
    """
    assert (isinstance(addr, Address) and "addr must be an Address object")
    symbol_table = currentProgram().getSymbolTable()
    sym_iter = symbol_table.getSymbolIterator()
    symbols = []
    while sym_iter.hasNext():
        sym = sym_iter.next()
        if sym.getAddress().getOffset() == addr.getOffset():
            symbols.append(sym)
    return symbols


def create_instruction_element(inst):
    """
    Creates a XML element for a given instruction, at the Heptane format

    :param inst: The instruction to convert

    :return: The XML element
    """
    asm = msp430_asm_to_heptane_fmt(inst.toString())
    inst_elem = ET.Element("INSTRUCTION", id=get_id(),
                           asm_type="Code", code=asm)
    attr_list = ET.SubElement(inst_elem, "ATTRS_LIST")
    addr = ET.SubElement(
        attr_list, "ATTR", type="address", name="address")
    access = ET.SubElement(
        addr, "ACCES", type="read", seg="code", varname="", precision="1")

    ET.SubElement(access, "ADDRSIZE", begin=hex(
        inst.getAddress().getOffset()), size=str(inst.getLength()))

    return inst_elem


def sanity_check(program):
    """
    Small sanity check, to see if the generated CFGs are valid
    """
    try:
        status = "FN_CALL"
        # Check function calls
        for node in program.findall('.//NODE[@type="FunctionCall"]'):
            assert ("called" in node.attrib)
            fn_name = node.attrib["called"]
            # Check that the called function exists
            assert (program.find(f'.//CFG[@name="{fn_name}"]') is not None)
        status = "CFG"
        # Check cfg
        for cfg in program.findall('.//CFG'):
            assert ("id" in cfg.attrib)
            assert ("startnode" in cfg.attrib)
            # assert("endnodes" in cfg.attrib)
            assert ("name" in cfg.attrib)
            # Check start node is valid
            start_node = cfg.attrib["startnode"]
            start_node_int = int(start_node)
            assert (start_node_int > 0)
            assert (program.find(f'.//NODE[@id="{start_node}"]') is not None)
    except AssertionError:
        import sys
        import traceback
        _, _, tb = sys.exc_info()
        traceback.print_tb(tb)  # Fixed format
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]

        logging.fatal('Sanity check failed line {} with statement {}'.format(line, text))
        if status == "FN_CALL":
            logging.fatal("Function call sanity check failed")
            logging.fatal(f"\tNode: {node.attrib}")
            if fn_name:
                logging.fatal(f"\tFunction called: {fn_name}")
            cfg = program.findall(f'.//NODE[@id="{node.attrib["id"]}"]/..')
            if len(cfg) > 0:
                logging.fatal(f"\tCFG: {cfg[0].attrib}")
        if status == "CFG":
            logging.fatal("CFG sanity check failed")
            logging.fatal("\tCFG: ", cfg.attrib)
        exit(1)


def populate_cfg(cfg, basic_blocks):
    """
    Populate the CFG element with the basic blocks and instructions
    """
    bb_id_by_addr = {}
    bb_id_by_addr.clear()
    cfg_entrypt_addr = int(cfg.attrib["cfgaddress"], base=16)

    for bb in basic_blocks:
        start_addr = bb.getFirstStartAddress().getOffset()
        bb_id = get_id()
        bb_elem = ET.SubElement(cfg, "NODE", id=bb_id, type="BasicBlock")

        # If the basic block contains a call, set the node type to "FunctionCall" (bb.getFlowType return UNKNOWN if there is more than one output flow)
        if bb.getFlowType().isCall() or bb.getNumDestinations(monitor()) > 1:
            # Find the called function
            dest_it = bb.getDestinations(monitor())
            while dest_it.hasNext():
                flow = dest_it.next()
                if flow.getFlowType().isCall():
                    called = flow.getDestinationBlock().getName()
                    bb_elem.set("type", "FunctionCall")
                    bb_elem.set("called", called)
                    # HOTFIX for the crc example -> Ghidra creates an alternative label
                    # for the __mspabi_slli_8 function
                    if called == "L0":
                        print("Found call to L0 in function: ", cfg.get("name"))
                        syms = get_symbols_at(
                            flow.getDestinationBlock().getFirstStartAddress())
                        for sym in syms:
                            if str(sym).startswith("__mspabi_"):
                                bb_elem.set("called", sym.getName())
                    break

        # Check if the basic block is the function entry point
        if start_addr == cfg_entrypt_addr:
            cfg.set("startnode", bb_id)

        # Check if the basic block is an exit point
        if bb.getFlowType().isTerminal():
            cfg.set("endnodes", cfg.get("endnodes", "") + bb_id + ", ")

        bb_id_by_addr[start_addr] = bb_id
        # Iterate over the instructions
        insts = currentProgram().getListing().getInstructions(bb, True)
        for inst in insts:
            # Sanity check, checkpoint should be labeled "FunctionCall" if it contains a call
            # TODO: Verify that the SimpleBlockModel cuts the basic blocks as expected, and
            #       remove this check
            if str(inst).lower().startswith("call") and not bb_elem.get("type") == "FunctionCall":
                logging.error(f"Found call in non-call basic block: {inst}")
                logging.error(f"Function: {cfg.get('name')} basic block: {bb.getName()}@{hex(start_addr)}")
                exit(1)

            bb_elem.append(create_instruction_element(inst))

    # Populate the cfg with the edges
    for bb in basic_blocks:
        start_addr = bb.getFirstStartAddress().getOffset()
        bb_id = bb_id_by_addr[start_addr]

        # Iterate over the successors
        succs = bb.getDestinations(monitor())
        while succs.hasNext():
            succ = succs.next().getDestinationBlock()
            succ_addr = succ.getFirstStartAddress().getOffset()
            # If the successor is in the CFG, add the edge
            if succ_addr in bb_id_by_addr:
                ET.SubElement(cfg, "EDGE", id=get_id(
                ), origin=bb_id, destination=bb_id_by_addr[succ_addr])

def select_fn_name(names):
    fname = names[0].getName()
    print("Available names: ", [name.getName() for name in names])
    for name in names:
        if name.getName().startswith("_"):
            fname = name.getName()
            break
    return fname

def clean_fn_names():
    for func in currentProgram().getListing().getFunctions(True):
        if not (func.getName().startswith(".") or func.getName().startswith("L0")):
            continue
        names = get_symbols_at(func.getEntryPoint())
        if len(names) == 1:
            continue
        else:
            old_name = func.getName()
            new_name = select_fn_name(names)
            for name in names:
                if name.getName() != new_name:
                    currentProgram().getSymbolTable().removeSymbolSpecial(name)
            logging.warning(f"Replacing function name {old_name} by {func.getName()}")

def get_called_fn(root, entrypoint):
    """
    Get the list of all the functions called from the entrypoint
    """
    called_fn = set([entrypoint])
    to_visit = [entrypoint]
    while len(to_visit) > 0:
        fn = to_visit.pop()
        cfg = root.find(f'.//CFG[@name="{fn}"]')
        if cfg is None:
            logging.fatal(f"Function {fn} not found in the binary, aborting")
            exit(1)

        for node in cfg.findall(f'.//NODE[@type="FunctionCall"]'):
            assert ("called" in node.attrib)
            if node.attrib["called"] == fn:
                logging.fatal(f"Found recursive call, aborting: {fn}")
                exit(1)
            called_fn.add(node.attrib["called"])
            to_visit.append(node.attrib["called"])

    return called_fn

def post_process(root, entrypoint):
    """
    Apply various post-processing to the generated XML

    - Remove the unused functions
    - Create a fake entry point if the entry point has an incoming edge (e.g entry point is a loop header). This adds a nop instruction.
    """
    called_fn = get_called_fn(root, entrypoint)

    for cfg in root.findall('.//CFG'):
        name = cfg.attrib["name"]
        if name not in called_fn:
            logging.warning(f"Removing unused function: {name}")
            root.remove(cfg)
            continue

        # Verify that cfg entry points does not have any incoming edge
        start_node_id = cfg.attrib["startnode"]
        if root.findall(f'.//EDGE[@destination="{start_node_id}"]'):
            new_start_id = get_id()
            new_start = ET.SubElement(cfg, "NODE", id=new_start_id, type="BasicBlock")

            # Get start address
            start_addr = cfg.attrib["cfgaddress"]
            # Create fake instruction
            inst = ET.SubElement(new_start, "INSTRUCTION", id=get_id(), asm_type="Code", code="nop")
            attr_list = ET.SubElement(inst, "ATTRS_LIST")
            addr = ET.SubElement(attr_list, "ATTR", type="address", name="address")
            access = ET.SubElement(addr, "ACCES", type="read", seg="code", varname="", precision="1")
            ET.SubElement(access, "ADDRSIZE", begin=start_addr, size=str(2))

            ET.SubElement(cfg, "EDGE", id=get_id(), origin=new_start_id, destination=start_node_id)
            cfg.attrib["startnode"] = new_start_id
            logging.warning(f"{name} entry point {new_start_id} has an incoming edge, adding a fake basic block")


def extract_program(name, entrypoint="main"):
    # create the root element
    root = ET.Element("PROGRAM", id="0", name=name)
    main_id = None
    # Keep track of the multiple-entry basic blocks traversed
    multi_entry_bb_hist = []

    clean_fn_names()

    functions = currentProgram().getListing().getFunctions(True)
    # iterate over the CFGs
    for func in natsorted(functions, key=lambda x: x.getName()):
        fname = func.getName()
        if fname in IGNORED_FUNCTIONS:
            print("Ignoring function: ", fname)
            continue

        fn_id = get_id()
        cfg_address = func.getEntryPoint().getOffset()


        if fname == entrypoint:
            main_id = fn_id

        # create the CFG element
        cfg_elem = ET.SubElement(root, "CFG", id=fn_id,
                                 name=fname, cfgaddress=f'0x{cfg_address:x}')

        blocks = []
        # Get the basic blocks
        block_itr = SimpleBlockIterator(SimpleBlockModel(
            currentProgram()), func.getBody(), monitor())
        # cfgaddress=cfg["cfgaddress"], startnode=cfg["startnode"], endnodes=cfg["endnodes"])

        while block_itr.hasNext():
            blocks.append(block_itr.next())

        # Make sure there are basic blocks in the function
        assert (len(blocks) > 0)

        # This part is a bit hacky
        # It assume that we analyze the functions in alphabetical order and that multiple-entry functions are
        # named __mspabi_XXXX_<INT>, with __mspabi_XXXX_1 the closest entry point to the function end

        # If the function name matches the __mspabi_XXXX_<INT> pattern, it means it is a multiple-entry function
        if re.match(r"__mspabi_.*_\d+", fname):
            # If this is the not first entry point, append the
            # append the blocks from the previous entry points
            if not re.match(r"__mspabi_.*_1$", fname):
                blocks = multi_entry_bb_hist + blocks
            multi_entry_bb_hist = blocks

        populate_cfg(cfg_elem, blocks)
        # # iterate over the nodes
        # for node in cfg["nodes"]:
        #     # create the NODE element

    # Ensure that the main function has been found
    assert (main_id is not None)
    root.set("entry", main_id)

    post_process(root, entrypoint)

    sanity_check(root)
    # Generate the symbol table attributes
    attr_list = ET.SubElement(root, "ATTRS_LIST")
    symbol_table = ET.SubElement(
        attr_list, "ATTR", type="symbolTable", name="symbolTable")
    sections = ET.SubElement(symbol_table, "SECTIONS")
    symbols = currentProgram().getSymbolTable()
    for section in currentProgram().getMemory().getBlocks():
        ET.SubElement(sections, "SECTION", name=section.getName(), begin=hex(
            section.getStart().getOffset()), size=str(section.getSize()))
    # create the xml file
    tree = ET.ElementTree(root)
    ET.indent(tree)

    tree.write("program.xml")


GREEN = "\033[92m"
NC = "\033[0m"

import logging

class CustomFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: grey + format + reset,
        logging.INFO: grey + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

import sys

if __name__ == "__main__":
    entrypoint = "main"
    args = getScriptArgs()

    logger = logging.getLogger()

    ch = logging.StreamHandler()
    ch.setFormatter(CustomFormatter())
    logger.addHandler(ch)

    if len(args) > 0:
        entrypoint = args[0]
    logging.info(f"Using entrypoint: {entrypoint}")
    # TODO: Get the program name from the command line, or via ghidra API
    progname = currentProgram().getName().replace(".exe", "").replace(".elf", "")
    print("Extracting program: ", progname, "...", end="")
    extract_program(progname, entrypoint)
    print(GREEN + "[DONE]" + NC)
