/* ---------------------------------------------------------------------

Copyright IRISA, 2003-2017

This file is part of Heptane, a tool for Worst-Case Execution Time (WCET)
estimation.
APP deposit IDDN.FR.001.510039.000.S.P.2003.000.10600

Heptane is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Heptane is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details (COPYING.txt).

See CREDITS.txt for credits of authorship

------------------------------------------------------------------------ */

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>
#include <queue>
// #include "Generic/CallGraph.h"
#include "Generic/Config.h"
#include "SharedAttributes/SharedAttributes.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/CheckpointIsolation/CustomIPETAnalysis.h"
#include "Specific/CheckpointIsolation/CustomDotPrint.h"

/// ----------------------
/// CheckpointIsolation class
///
/// This class is used to compute the energy need to go from one checkpoint to
/// the next ones in the worst case.
///
/// The analysis iterates over the checkpoints of the program.
/// For each checkpoint, it:
/// - Find the instructions after the checkpoint
/// - Compute the subgraph of the edges/nodes reachable from the checkpoint
/// - Launch an IPET analysis on this subgraph
///
/// To do so, we modified the IPET analysis with the attribute "EdgeDeactivated"
///
/// @param output_file the csv output filename
/// ---------------------------------------------------------------------------
CheckpointIsolation::CheckpointIsolation(Program *p, string output_file)
    : Analysis(p)
{
  if (output_file.size() == 0)
  {
    output_file = "checkpoint_energy.csv";
  }
  // Set the output file
  this->output_file = output_file;
}

/// ----------------------------------------------------------------
/// Checks if all required attributes are in the CFG
/// Returns true if successful, false otherwise
/// Here, we simply check that there is a cfg to print
/// ----------------------------------------------------------------
bool CheckpointIsolation::CheckInputAttributes()
{
  // Stub
  return true;
}

/// ----------------------------------------------------------------
/// Check if the given basic block contains a call to
/// the checkpoint function
///
/// @param bb the basic block to check
/// @return true if the basic block contains a call to the checkpoint
/// function, false otherwise
/// ----------------------------------------------------------------
bool CheckpointIsolation::ContainCheckpointCall(Node *bb)
{
#ifndef TEST
  return bb->IsCall() && bb->GetCallee()->GetName().back() == "checkpoint";
#else
  return bb->HasAttribute(CheckpointBB);
#endif
}

/// ----------------------------------------------
/// Find basic blocks where the checkpoint function is called in the program
///
/// @param p the program to analyse
/// @return a list of basic blocks containing calls to "checkpoint"
///
/// ----------------------------------------------
vector<Node *> CheckpointIsolation::GetCheckpointList(Program *p)
{
  vector<Node *> checkpoints;
  // For all functions
  for (Cfg *cfg : p->GetAllCfgs())
  {
    // For all basic blocks containing call to "checkpoint"
    for (Node *bb : AnalysisHelper::getCheckpointNodes(cfg))
    {
      checkpoints.push_back(bb);
    }
  }
  return checkpoints;
}

/// ----------------------------------------------
/// Get the address of a checkpoint
///
/// Assumes that the checkpoint call is the last
/// instruction of the basic block
///
/// @param checkpoint the basic block containing the call to checkpoint
/// @return the address of the checkpoint
///
/// ----------------------------------------------
long getCheckpointAddress(Node *checkpoint)
{
  Instruction *call = checkpoint->GetInstructions().back();
  assert(call->HasAttribute(AddressAttributeName) &&
         "Call instruction does not have an address attribute");
  AddressAttribute ba =
      (AddressAttribute &)call->GetAttribute(AddressAttributeName);
  return ba.getCodeAddress();
}

/// ----------------------------------------------
/// Launch an IPET analysis, and return the WCET
///
/// Launch an IPET analysis with CPLEX, no pipeline, generating WCET info and
/// the node frequency After the analysis, the attributes used by the IPET
/// analysis are removed.
///
/// However, frequency and WCET attributes are kept, make sure to clean them before
/// launching another analysis
///
/// @param p the program to analyse
/// @return the computed WCET
///
///
/// ----------------------------------------------

int CheckpointIsolation::launchIpetAnalysis(Program *p, Node *starting_point)
{
  // Save the old entry point and the old start node of the function
  Cfg *old_entry_point = p->GetEntryPoint();
  Cfg *starting_function = starting_point->GetCfg();
  if (starting_point != old_entry_point->GetStartNode())
  {
    AnalysisHelper::SetBoolAttr(old_entry_point, FunctionDeactivated);
    cout << "Deactivating function " << old_entry_point->GetName().back() << endl;
  }
  // Set the entry point to the function containing the checkpoint
  p->SetEntryPoint(starting_function);
  AnalysisHelper::SetBoolAttr(starting_point, AnalysisStartingPointAttribute);
  /// Create the IPET analysis
  CustomIPETAnalysis *ipet = new CustomIPETAnalysis(
      p, CPLEX, false, true, true, config->getNbICacheLevels(),
      config->getNbDCacheLevels(), config->GetCachesNotConst());
  /// Check if the required attributes are present
  if (!ipet->CheckInputAttributes())
  {
    Logger::addFatal("Checkpoint Isolation Analysis: Error when checking "
                     "attributes for IPET analysis, exiting");
  }

  // Perform the analysis
  bool res = ipet->PerformAnalysis();
  if (!res)
  {
    Logger::addFatal("Analysis: Error when performing analysis, exiting");
  }

   // TODO: the way we get the checkpoint name is hacky, we should find a better way
  bool dot_print = true; // TODO make it a config option
  if (dot_print)
  {
    vector<Node *> predecessors = starting_function->GetPredecessors(starting_point);
    Node *checkpoint = nullptr;
    for (Node *pred : predecessors)
    {
      if (ContainCheckpointCall(pred))
      {
        checkpoint = pred;
        break;
      }
    }

    // Create the folder that will store cfg of the analysis
    stringstream folder_name;
    folder_name << "checkpoint_analysis/";
    if (checkpoint == nullptr)
    {
      folder_name << "Start";
    }
    else
    {
      long address = getCheckpointAddress(checkpoint);
      folder_name << "0x" << hex << address;
    }

    string folder = folder_name.str();
    string cmd = "mkdir -p " + folder;
    if (system(cmd.c_str()))
    {
      Logger::addFatal(string("CheckpointIsolation: Error when creating ") + folder + " folder");
    }

    CustomDotPrint print_pass = CustomDotPrint(p, folder, true);
    print_pass.PerformAnalysis();
    print_pass.RemovePrivateAttributes();
  }
  // Get the WCET value
  Cfg *main = p->GetEntryPoint();
  assert(main->HasAttribute(WCETAttributeName) &&
         "main function does not have WCET attribute");
  SerialisableStringAttribute ba =
      (SerialisableStringAttribute &)main->GetAttribute(WCETAttributeName);
  string WCET = ba.GetValue();

  long wcetValue = -1;
  try
  {
    wcetValue = stol(WCET);
  }
  catch (const std::invalid_argument &ia)
  {
    Logger::addFatal("Analysis: Error when converting WCET to long. WCET = " + WCET + ", exiting");
  }


  // Restore the old entry point and the old start node of the function
  p->SetEntryPoint(old_entry_point);
  ipet->RemovePrivateAttributes();

  return wcetValue;
}

/// ----------------------------------------------
/// Deactivate edges that are not reachable from
/// the checkpoint without encountering another checkpoint
///
/// @param p the program to analyse
/// @param starting_point the checkpoint from which we start the analysis
/// @return the number of deactivated edges
/// ----------------------------------------------
int CheckpointIsolation::DeactivateUnreachableEdges(Program *p, Node *starting_point)
{
  Cfg * start_function = starting_point->GetCfg();

  bool in_entrypt_subtree = false;
  queue<Node *> to_visit;
  set<Node *> visited;
  set<Edge *> activated_edges;
  set<Cfg *> reachable_functions;
  set<Node *> loop_headers = AnalysisHelper::getLoopHeadersSet(p);
  // Add the checkpoint to the queue
  to_visit.push(starting_point);
  reachable_functions.insert(starting_point->GetCfg());
  // While there are still nodes to visit
  while (to_visit.size())
  {
    Node *current_bb = to_visit.front();
    to_visit.pop();
    Cfg *current_function = current_bb->GetCfg();

    // Add the node to the visited set
    visited.insert(current_bb);
    // If this bb ends with a call to a function
    // Start discovering the function and stop exploring the bb
    // successors
    if (current_bb->IsCall())
    {
      // If the call comes from the entry point, we are in its calling tree
      if(current_function == start_function) {
        in_entrypt_subtree = true;
      }
      // If the current bb is a call to another function, add all the edges
      // of the function to the activated edges
      Node *func_start = current_bb->GetCallee()->GetStartNode();
      assert(func_start != nullptr && "Basic block call external function, this is not supported yet");
      reachable_functions.insert(current_bb->GetCallee());
      if (visited.find(func_start) == visited.end())
        to_visit.push(func_start);
    }

    // If the curent bb is a call to checkpoint, stop the traversal
    if (ContainCheckpointCall(current_bb))
    {
      reachable_functions.insert(p->GetCfgByName("checkpoint"));
      continue;
    }
    // If the current bb is a return, follow this return if we
    // might resume the execution from the current function
    if (current_bb->IsReturn() && !in_entrypt_subtree)
    {

      AnalysisHelper::getCallerNodes(current_function);
      for (Node *caller : AnalysisHelper::getCallerNodes(current_function))
      {
        reachable_functions.insert(caller->GetCfg());
        for (Node *return_point : caller->GetCfg()->GetSuccessors(caller))
        {
          if (visited.find(return_point) == visited.end())
            to_visit.push(return_point);
        }
      }
      continue;
    }

    // For all successors of the current node
    for (Edge *e : current_function->GetOutgoingEdges(current_bb))
    {
      activated_edges.insert(e);
      // If the successor has not been visited yet, add it to the queue
      if (visited.find(e->GetTarget()) == visited.end())
        to_visit.push(e->GetTarget());
    }
  }
  // Deactivate all edges that are not in the activated edges set
  int nb_deactivated_edges = 0;
  for (Cfg *cfg : p->GetAllCfgs())
  {
    if (reachable_functions.find(cfg) == reachable_functions.end())
    {
      AnalysisHelper::SetBoolAttr(cfg, FunctionDeactivated);
      cout << "Deactivating function " << cfg->GetName().back() << endl;
    }

    for (Edge *e : cfg->GetAllEdges())
    {
      // If the edge is not in the activated edges, deactivate it
      if (activated_edges.find(e) == activated_edges.end())
      {
        SerialisableIntegerAttribute edgeDeactivated(1);
        e->SetAttribute(EdgeDeactivated, edgeDeactivated);
        nb_deactivated_edges++;
      }
      if (e->GetSource()->IsCall())
      {
        AnalysisHelper::SetBoolAttr(e, EdgeDeactivated);
      }
    }
  }
  return nb_deactivated_edges;
}

vector<string> CheckpointIsolation::GetPathWithNodeFrequency(Program *p, Node *starting_point)
{
  vector<string> nodes_visited;
  queue<Node *> to_visit;
  set<Node *> visited;
  if (starting_point == nullptr)
  {
    starting_point = p->GetEntryPoint()->GetStartNode();
  }
  to_visit.push(starting_point);

  while (to_visit.size())
  {
    Node *current_bb = to_visit.front();
    to_visit.pop();
    assert(current_bb != nullptr);
    visited.insert(current_bb);
    int freq = 0;
    for (string attr : current_bb->getAttributeList())
    {
      if (attr.rfind(FrequencyAttributeName) == 0)
      {
        auto frequency = (SerialisableUnsignedLongAttribute &)current_bb->GetAttribute(attr);
        freq += frequency.GetValue();
      }
    }

    if (freq == 0)
      continue;

    ostringstream stream;
    stream << "0x" << hex << AnalysisHelper::getStartAddress(current_bb);
    string addr = stream.str();
    stream.clear();
    nodes_visited.push_back(addr);
    if (current_bb->IsCall())
    {
      if (visited.find(current_bb->GetCallee()->GetStartNode()) == visited.end())
        to_visit.push(current_bb->GetCallee()->GetStartNode());
    }
    for (Edge *e : current_bb->GetCfg()->GetOutgoingEdges(current_bb))
    {
      if (visited.find(e->GetTarget()) == visited.end())
        to_visit.push(e->GetTarget());
    }
  }

  return nodes_visited;
}

/// ----------------------------------------------
/// Analyse the program from a given checkpoint
///
/// @param p the program to analyse
/// @param starting_point the checkpoint from which we start the analysis
/// @return the WCET of the program and the longest path
/// ----------------------------------------------
IPETResult CheckpointIsolation::AnalyseFromCheckpoint(Program *p,
                                                      Node *checkpoint)
{
  assert(p != nullptr && "Program is null");
  Node *start_node;
  // If checkpoint is null, we start from the program entry point
  if (checkpoint == nullptr)
  {
    start_node = p->GetEntryPoint()->GetStartNode();
  }
  else
  {
    // Get the checkpoint successors
    vector<Node *> successors = checkpoint->GetCfg()->GetSuccessors(checkpoint);
    assert(successors.size() != 0 &&
           "A checkpoint does not have any successor");
    assert(successors.size() == 1 &&
           "We do not handle checkpoints with more than one bb successor");
    start_node = successors[0];
  }
  AnalysisHelper::SetBoolAttr(start_node, AnalysisStartingPointAttribute);
  DeactivateUnreachableEdges(p, start_node);
  // DeactivateUnreachableBB(p);
  // Get the WCET of the program
  long wcetValue = launchIpetAnalysis(p, start_node);
  vector<string> longest_path = GetPathWithNodeFrequency(p, start_node);
  return make_tuple(wcetValue, longest_path);
}

/// ----------------------------------------------
/// Performs the analysis
///
/// 1. Find all calls to "checkpoint" in the program
/// 2. Analyse the program for each checkpoint
///   2.1 Find the successors of the checkpoint
///   2.2 Deactivate all edges that are not reachable from the checkpoint
///   2.3 Compute the WCET of the subgraph of all the edges still active,
///   starting from the checkpoint
/// 3. Log the WCET from each checkpoint in the given output file
///
/// Returns true if successful, false otherwise
/// ----------------------------------------------
bool CheckpointIsolation::PerformAnalysis()
{
  vector<CheckpointResult> checkpoints_results;
  // List all CFGs
  vector<Node *> chkptList = GetCheckpointList(p);
  chkptList.push_back(nullptr);
  for (Node *chkpt : chkptList)
  {
    // Get the name of the checkpoint
    // If the checkpoint is null, we start from the program entry point
    // Else, we get the address of the checkpoint
    string chkpt_name = "Start";
    if (chkpt != nullptr)
    {
      stringstream ss;
      long address = getCheckpointAddress(chkpt);
      ss << "0x" << hex << address;
      chkpt_name = ss.str();
    }

    // Analyse the program from the checkpoint
    cout << "[CheckpointEstimation] Analyzing cost from " << chkpt_name << endl;
    IPETResult analysis_result = AnalyseFromCheckpoint(p, chkpt);
    cout << " WCET = " << get<0>(analysis_result) << endl;
    CheckpointResult result = make_tuple(chkpt_name,
                                         get<0>(analysis_result),
                                         get<1>(analysis_result));
    checkpoints_results.push_back(result);

    RemovePrivateAttributes();
  }

  // Print the results in the output file
  ofstream output;
  output.open(this->output_file);
  cout << "[CheckpointEstimation] Writing results in " << this->output_file
       << endl;
  output << "Checkpoint; WCET; Path;" << endl;
  for (CheckpointResult result : checkpoints_results)
  {
    output << get<0>(result) << ";" << to_string(get<1>(result)) << ";";
    vector<string> path = get<2>(result);
    for (size_t i = 0; i < path.size(); i++)
    {
      output << path[i];
      if (i != path.size() - 1)
        output << "-";
    }
    output << ";" << endl;
  }
  output.close();
  RemovePrivateAttributes();
  return true;
}

/// ----------------------------------------------
///
/// Remove the attributes added by our analysis, the IPET analysis
/// and the solver (WCETAttributeName, WCET, frequency)
///
/// @param ipet
/// @param p
///
/// ----------------------------------------------
void CheckpointIsolation::RemovePrivateAttributes()
{
  // Remove all the WCETAttributeName from CFG, if applicable
  for (Cfg *cfg : p->GetAllCfgs())
  {
    if (cfg->HasAttribute(WCETAttributeName))
      cfg->RemoveAttribute(WCETAttributeName);
    if (cfg->HasAttribute("WCET"))
      cfg->RemoveAttribute("WCET");
    if (cfg->HasAttribute(FunctionDeactivated))
      cfg->RemoveAttribute(FunctionDeactivated);

    for (Edge *e : cfg->GetAllEdges())
    {
      if (e->HasAttribute(EdgeDeactivated))
        e->RemoveAttribute(EdgeDeactivated);
      if (e->HasAttribute(EntryPointEdge))
        e->RemoveAttribute(EntryPointEdge);
    }

    for (Node *bb : cfg->GetAllNodes())
    {
      if (bb->HasAttribute(AnalysisStartingPointAttribute))
        bb->RemoveAttribute(AnalysisStartingPointAttribute);
      if (bb->HasAttribute(NodeDeactivated))
        bb->RemoveAttribute(NodeDeactivated);
      for (string attr : bb->getAttributeList())
      {
        if (attr.rfind(FrequencyAttributeName) == 0)
        {
          bb->RemoveAttribute(attr);
        }
      }
    }
  }
}
