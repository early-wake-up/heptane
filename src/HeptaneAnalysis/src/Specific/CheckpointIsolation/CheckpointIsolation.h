/* ---------------------------------------------------------------------

Copyright IRISA, 2003-2017

This file is part of Heptane, a tool for Worst-Case Execution Time (WCET)
estimation.
APP deposit IDDN.FR.001.510039.000.S.P.2003.000.10600

Heptane is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Heptane is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details (COPYING.txt).

See CREDITS.txt for credits of authorship

------------------------------------------------------------------------ */

#ifndef CHECKPOINT_ISOLATION_H
#define CHECKPOINT_ISOLATION_H

#include "Analysis.h"

// CheckpointResult is a tuple containing the checkpoint address and its WCET
typedef tuple<string, long, vector<string>> CheckpointResult;

// IPET analysis result
typedef tuple<long, vector<string>> IPETResult;

/**
 * This analysis is defined for developers only
 * in order to avoid the definition of a new analysis.
 *
 * That one can be used directly
 *
 * Used in:
 *  - GNUmakefile
 *  - Generic/Config.h
 *  - Generic/Config.cc
 *
 */
class CheckpointIsolation : public Analysis
{
private:
  string output_file;

  IPETResult AnalyseFromCheckpoint(Program *p,
                                   Node *checkpoint);

public:
  /* Constructor */
  CheckpointIsolation(Program *p, string output_file);

  /** Checks if all required attributes are in the CFG
      @return always true.
  */
  bool CheckInputAttributes();
  vector<Node *> GetCheckpointList(Program *p);
  bool ContainCheckpointCall(Node *bb);

  /// ----------------------------------------------
  /// Launch an IPET analysis, and return the WCET
  ///
  /// Launch an IPET analysis with CPLEX, no pipeline, generating WCET info and
  /// the node frequency After the analysis, the attributes used by the IPET
  /// analysis are removed.
  ///
  /// However, frequency and WCET attributes are kept, make sure to clean them before
  /// launching another analysis
  ///
  /// @param p the program to analyse
  /// @return the computed WCET
  ///
  ///
  /// ----------------------------------------------
  int launchIpetAnalysis(Program *p, Node *starting_point);

  /// ----------------------------------------------
  /// Deactivate edges that are not reachable from
  /// the checkpoint without encountering another checkpoint
  ///
  /// @param p the program to analyse
  /// @param starting_point the checkpoint from which we start the analysis
  /// @return the number of deactivated edges
  /// ----------------------------------------------
  int DeactivateUnreachableEdges(Program *p, Node *starting_point);

  void DeactivateUnreachableBB(Program *p);
  vector<string> GetPathWithNodeFrequency(Program *p, Node *starting_point);
  /** Performs the analysis
      @return always true.
  */
  bool PerformAnalysis();

  /** Remove all private attributes */
  void RemovePrivateAttributes();
};

#endif
