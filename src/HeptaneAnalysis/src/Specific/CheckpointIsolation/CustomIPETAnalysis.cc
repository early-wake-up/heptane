/* ---------------------------------------------------------------------

   Copyright IRISA, 2003-2017

   This file is part of Heptane, a tool for Worst-Case Execution Time (WCET)
   estimation.
   APP deposit IDDN.FR.001.510039.000.S.P.2003.000.10600

   Heptane is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Heptane is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details (COPYING.txt).

   See CREDITS.txt for credits of authorship

   ------------------------------------------------------------------------ */

/* -----------------------------------------------------

   Generate an ILP system for this program. (NB: not yet integrated with
   pipeline analysis)

   The IPET computation is contextual. Contextual in this
   context means that different contexts are identified for every BB
   depending on its call path. Contextual IPET generate ILP constraints
   for every call context. Naming conventions of contexts are explained
   in file SharedAttributes/SharedAttributes.h.

   Variables of the ILP system:

   - execution frequency for first and next executions of every basic
   block, and total. First means the very execution of the basic block
   (even if the BB is in deeply nested loops). There is a pair (ffirst,fnext) per call context.

   - execution frequency for edges between basic blocks. There is a value per context.

   Naming conventions:

   - BB execution frequencies: n_NID_cCNB (ex: n_0_c0 for the execution
   frequency of BB number zero in execution context 0). prefixes nf and
   nn stand for first and next execution counts of a BB (ex: n_0_c0 =
   nf_0_c0 + nn_0_c0). Context numbers are used instead of context
   names because of the length of context names and the presence of
   characters likely to not be supported as variable identifiers in ILP
   solvers.

   - edge execution frequencies. Similar naming conventions are used:
   e_NID_NID_cCNB (ex: e_0_1_c0 for an edge between node 0 and node 1
   in call context number 0).

   Internal attributes used:

   - Node identifiers (InternalAttributeId) attached to every BB:
   nodes are identified using integers starting at 0

   - WCET of first and next executions of every BB for every call context
   of the node (name InternalAttributeWCETfirst+"#"+contextName and
   respectively InternalAttributeWCETnext+"#"+contextName, where
   contextName is the context name in case of contextual IPET)

   Attributes used as inputs:

   - Loop maxiter

   - Cache classification. A cache classification should be attached to
   every instruction for every cache level (see SharedAttributes.h).
   The attribute name is: BaseAttrName+"#"+contextName.

   -------------------------------------------------------- */

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <cassert>

#include "Analysis.h"
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CustomIPETAnalysis.h"
#include "Specific/CheckpointIsolation/CustomSolver.h"
#include "SharedAttributes/SharedAttributes.h"

#include "arch.h"
#include "Utl.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <regex>
#include <queue>

/*  METHOD_NOPIPELINE_ICACHE_DCACHE: generates 2 variables per BB (freq_first + freq_next) by context
 *  METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE: does not consider cache analysis results (simply counts 1 cycle per instruction)
 *  METHOD_PIPELINE_ICACHE_DCACHE: generates 2 variables per BB (freq_first + freq_next) by context
 *     The WCET value per BB is given by the PipelineAnalysis method.
 */
#define NOT_YET_IMPLEMENTED -1
#define METHOD_PIPELINE_ICACHE_DCACHE 1
#define METHOD_PIPELINE_ICACHE_PERFECTDCACHE 2
#define METHOD_NOPIPELINE_ICACHE_DCACHE 3
#define METHOD_NOPIPELINE_ICACHE_PERFECTDCACHE 4
#define METHOD_NOPIPELINE_PERFECTICACHE_DCACHE 5
#define METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE 6

#define isNULLPointer(n) (n == NULL)

void CustomIPETAnalysis::printBasicBlockInfos(Node *n, string vtext)
{
  t_address add = AnalysisHelper::getStartAddress(n);
  cout << "=====>>>>> " << vtext << " Basic Block @0x" << std::hex << add << " " << std::dec << " <<<<<=====" << endl;
}

// NOW we should have m = METHOD_[PIPELINE, NOPIPELINE]_[ICACHE, PERFECT_ICACHE]_[ DCACHE | PERFECT_DCAHE] ]

int CustomIPETAnalysis::getIPETMethodToApply(bool pipeline, bool perfectIcache, bool perfectDcache)
{
  if (pipeline)
  {
    if (!perfectIcache)
    {
      Logger::print("*** IPET analysis, the DATA CACHE is ignored in the PIPELINE analysis.");
      if (!perfectDcache)
        return METHOD_PIPELINE_ICACHE_DCACHE;
      return METHOD_PIPELINE_ICACHE_PERFECTDCACHE;
    }
    else
    {
      Logger::addFatal("*** IPET analysis: Pipeline without instruction cache is not implemented");
      return NOT_YET_IMPLEMENTED;
    }
  }
  else
  {
    if (!perfectIcache)
    {
      if (!perfectDcache)
        return METHOD_NOPIPELINE_ICACHE_DCACHE;
      return METHOD_NOPIPELINE_ICACHE_PERFECTDCACHE;
    }
    else
    {
      if (!perfectDcache)
        return METHOD_NOPIPELINE_PERFECTICACHE_DCACHE;
      return METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE;
    }
  }
  return NOT_YET_IMPLEMENTED;
}

// ---------------------------------------
// Constructor
// -----------
// - p: program whose WCET is to be computed
// - m: WCET computation method (METHOD_NOPIPELINE_ICACHE_DCACHE, METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE, METHOD_PIPELINE_ICACHE_DCACHE, ....)
// - used_solver: used solver (LP_SOLVE or CPLEX)
// - generate_wcet_info: true if WCET information is attached to the CFG of entry point
// - generate_node_freq: true if frequency information is attached to the nodes (one value per execution context)
// - latencyPerfectIcache : useful only for PerfectIcache method
// - latencyPerfectDcache : useful only for PerfectDcache method
// ---------------------------------------
CustomIPETAnalysis::CustomIPETAnalysis(Program *p, int used_solver, bool pipeline, bool generate_wcet_info, bool generate_node_freq, int nb_icache_levels,
                                       int nb_dcache_levels, map<int, vector<CacheParam *>> &cache_params) : Analysis(p)
{
  bool perfectDcache = false;
  bool perfectIcache = false;

  // Check solver parameter is correct and create associated object
  assert(used_solver == LP_SOLVE || used_solver == CPLEX);
  if (used_solver == LP_SOLVE)
    solver = new LpsolveCustomSolver((CustomIPETAnalysis *)this);
  else
    solver = new CPLEXCustomSolver((CustomIPETAnalysis *)this);

  // Fill-in member variables from parameters
  generate_wcet_information = generate_wcet_info;
  generate_node_frequencies = generate_node_freq;
  NbICacheLevels = nb_icache_levels;
  NbDCacheLevels = nb_dcache_levels;
  MemoryStoreLatency = config->getMemoryStoreLatency();
  MemoryLoadLatency = config->getMemoryLoadLatency();

  this->call_graph = new CallGraph(p);

  PerfectICacheLatency = 0;
  // Fill-in attribute names for the different instruction and cache levels
  for (int l = 1; l <= NbICacheLevels; l++)
  {
    CodeCHMC[l] = CHMCAttributeNameCode(l);
    t_cache_type tCache = cache_params[l][0]->type;
    if (tCache == ICACHE || tCache == PERFECTICACHE)
      levelAccessCostInstr[l] = cache_params[l][0]->latency;
    else
    {
      tCache = cache_params[l][1]->type;
      levelAccessCostInstr[l] = cache_params[l][1]->latency;
    }
    if (tCache == PERFECTICACHE)
    {
      PerfectICacheLatency = levelAccessCostInstr[l];
      perfectIcache = true;
    }
  }

  PerfectDCacheLatency = 0;
  for (int l = 1; l <= NbDCacheLevels; l++)
  {
    DataCHMC[l] = CHMCAttributeNameData(l);
    blockCountName[l] = BlockCountAttributeName(l);
    t_cache_type tCache = cache_params[l][0]->type;
    if (tCache == DCACHE || tCache == PERFECTDCACHE)
      levelAccessCostData[l] = cache_params[l][0]->latency;
    else
    {
      tCache = cache_params[l][1]->type;
      levelAccessCostData[l] = cache_params[l][1]->latency;
    }
    if (tCache == PERFECTDCACHE)
    {
      PerfectDCacheLatency = levelAccessCostData[l];
      perfectDcache = true;
    }
  }

  method = getIPETMethodToApply(pipeline, perfectIcache, perfectDcache);
  // printIPETCommand();

  // Fill-in attribute name of the different delta
  deltaName.push_back(DeltaFFAttributeName);
  deltaName.push_back(DeltaFNAttributeName);
  deltaName.push_back(DeltaNFAttributeName);
  deltaName.push_back(DeltaNNAttributeName);
  TRACE(cout << " CustomIPETAnalysis::CustomIPETAnalysis () : END " << endl);
  TRACE(cout << " ############################################################################" << endl);
}

// ---------------------------------------------------
// Removal of private attributes
// (node identifiers, WCET for first and next iterations)
// ---------------------------------------------------
static bool CleanupNodeInternalAttributes(Cfg *c, Node *n, void *param)
{
  string contextName, attr;
  // Remove InternalAttributeId (used to generate names for constraints in the ILP system)
  if (n->HasAttribute(InternalAttributeId))
    n->RemoveAttribute(InternalAttributeId);

  // Remove WCETs for first and next iterations, for all contexts
  const ContextList &contexts = (ContextList &)c->GetAttribute(ContextListAttributeName);
  unsigned int nc = contexts.size();
  for (unsigned int ic = 0; ic < nc; ic++)
  {
    contextName = contexts[ic]->getStringId();
    attr = AnalysisHelper::mkContextAttrName(InternalAttributeWCETfirst, contextName);
    if (n->HasAttribute(attr))
      n->RemoveAttribute(attr);

    attr = AnalysisHelper::mkContextAttrName(InternalAttributeWCETnext, contextName);
    if (n->HasAttribute(attr))
      n->RemoveAttribute(attr);
  }

  return true;
}

void CustomIPETAnalysis::RemovePrivateAttributes()
{
  AnalysisHelper::applyToAllNodesRecursive(p, CleanupNodeInternalAttributes, (void *)(this));
}

// -----------------------------------------------------
// Used to check the validity of cache attribute on an instruction
//
// Checks that attribute of name attr_name is attached
// to the instruction and contains valid values only
// NB: the attribute name is a parameter because there is
//     an attribute name per context (base#context)
// -----------------------------------------------------
bool CustomIPETAnalysis::CheckCacheAttributes(Instruction *i, string attr_name)
{

  // Check the attribute is attached

  // TODO Find a better way to check attribute correctness and still be able not to
  // perform either the instruction or the data cache analysis.
  /* if (i->HasAttribute(attr_name)==false) {
     stringstream errorstr;
     errorstr << "CustomIPETAnalysis: "Instruction (" << i->GetCode()
     << ") should have a classification (AH/AM/NC/FM/AU)";
     Logger::addFatal(errorstr.str());
     return false;
     } else {
     // Validity check of attribute contents
     SerialisableStringAttribute att = (SerialisableStringAttribute &) i->GetAttribute(attr_name);
     string s=att.GetValue();
     if (s != "AH" && s != "AM" && s != "NC" && s != "FM" && s != "AU") {
     stringstream errorstr;
     errorstr << "CustomIPETAnalysis: Invalid classification for instr (" << i->GetCode()
     << "): " << s << " Valid ones are (AH/AM/NC/FM/AU)";
     Logger::addFatal(errorstr.str());
     return false;
     }
     }
   */

  if (!i->HasAttribute(attr_name))
    return false;
  // Validity check of attribute contents
  SerialisableStringAttribute att = (SerialisableStringAttribute &)i->GetAttribute(attr_name);
  string s = att.GetValue();
  if (s != "AH" && s != "AM" && s != "NC" && s != "FM" && s != "AU")
    return false;
  return true;
}

/* Check all executed instructions have a cache classification */
bool CustomIPETAnalysis::CheckInputAttributesCacheClassification(Cfg *aCfg)
{
  Node *currentNode;
  bool requireDCacheAttr, requireICacheAttr;
  Instruction *vinstr;
  string attributeName;
  string vcontext;

  if (method != METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE)
  {

    if (!isDeadCode(aCfg))
    {
      requireDCacheAttr = (method == METHOD_NOPIPELINE_ICACHE_DCACHE) || (method == METHOD_NOPIPELINE_PERFECTICACHE_DCACHE) || (method == METHOD_PIPELINE_ICACHE_DCACHE);
      requireICacheAttr = (method == METHOD_NOPIPELINE_ICACHE_DCACHE) || (method == METHOD_NOPIPELINE_ICACHE_PERFECTDCACHE) || (method == METHOD_PIPELINE_ICACHE_DCACHE);

      const ContextList &contexts = (ContextList &)aCfg->GetAttribute(ContextListAttributeName);
      unsigned int nc = contexts.size();
      vector<Node *> vn = aCfg->GetAllNodes();
      for (unsigned int n = 0; n < vn.size(); n++)
      {
        currentNode = vn[n];
        if (currentNode->IsBB())
          if (!currentNode->isIsolatedNopNode())
          {
            vector<Instruction *> vi = currentNode->GetInstructions();
            for (unsigned int inst = 0; inst < vi.size(); inst++)
            {
              vinstr = vi[inst];
              if (vinstr->IsCode())
              {
                for (int currentCacheLevel = 1; currentCacheLevel <= NbICacheLevels; currentCacheLevel++)
                {
                  for (unsigned int ic = 0; ic < nc; ic++)
                  {
                    vcontext = contexts[ic]->getStringId();
                    if (requireICacheAttr)
                    {
                      attributeName = AnalysisHelper::mkContextAttrName(CodeCHMC[currentCacheLevel], vcontext);
                      if (!this->CheckCacheAttributes(vinstr, attributeName))
                        return false;
                    }
                    if (requireDCacheAttr)
                    {
                      attributeName = AnalysisHelper::mkContextAttrName(DataCHMC[currentCacheLevel], vcontext);
                      if (!this->CheckCacheAttributes(vinstr, attributeName))
                        return false;
                    }
                  }
                }
              }
            }
          }
      }
    }
  }
  return true;
}

/*
   Check the pipeline timing provided by the pipelineAnalysis(nodes and edges)
*/
bool CustomIPETAnalysis::CheckInputAttributesPipelineTiming(Cfg *aCfg)
{
  if ((method == METHOD_PIPELINE_ICACHE_DCACHE) || (method == METHOD_PIPELINE_ICACHE_PERFECTDCACHE))
  {
    vector<Node *> vn = aCfg->GetAllNodes();
    const ContextList &contexts = (ContextList &)aCfg->GetAttribute(ContextListAttributeName);
    unsigned int nc = contexts.size();
    for (std::vector<Node *>::iterator itNode = vn.begin(); itNode != vn.end(); itNode++)
    {
      // for each context
      for (unsigned int ic = 0; ic < nc; ic++)
      {
        if (!(*itNode)->isIsolatedNopNode())
        {
          string contextName = contexts[ic]->getStringId();
          string attributeNameFirst = AnalysisHelper::mkContextAttrName(NodeExecTimeFirstAttributeName, contextName);
          string attributeNameNext = AnalysisHelper::mkContextAttrName(NodeExecTimeNextAttributeName, contextName);
          if ((*itNode)->HasAttribute(attributeNameFirst) == false || (*itNode)->HasAttribute(attributeNameNext) == false)
          {
            stringstream errorstr;
            errorstr << "CustomIPETAnalysis: Nodes should have " << NodeExecTimeFirstAttributeName << " attribute set."
                     << "(this attribute is provided with the PipelineAnalysis)";
            Logger::addFatal(errorstr.str());
            return false;
          }
        }
      }
    }

    // edges
    vector<Edge *> ve = aCfg->GetAllEdges();
    for (std::vector<Edge *>::iterator itEdge = ve.begin(); itEdge != ve.end(); itEdge++)
    {
      for (unsigned int ic = 0; ic < nc; ic++)
      {
        string contextName = contexts[ic]->getStringId();
        string attributeNameFF = AnalysisHelper::mkContextAttrName(DeltaFFAttributeName, contextName);
        string attributeNameFN = AnalysisHelper::mkContextAttrName(DeltaFNAttributeName, contextName);
        string attributeNameNF = AnalysisHelper::mkContextAttrName(DeltaNFAttributeName, contextName);
        string attributeNameNN = AnalysisHelper::mkContextAttrName(DeltaNNAttributeName, contextName);
        if ((*itEdge)->HasAttribute(attributeNameFF) == false || (*itEdge)->HasAttribute(attributeNameFN) == false ||
            (*itEdge)->HasAttribute(attributeNameNF) == false || (*itEdge)->HasAttribute(attributeNameNN) == false)
        {
          stringstream errorstr;
          errorstr << "CustomIPETAnalysis: Nodes should have " << DeltaFFAttributeName << " attribute set."
                   << "(this attribute is provided with the PipelineAnalysis)";
          Logger::addFatal(errorstr.str());
          return false;
        }
      }
    }
  }
  return true;
}

// ----------------------------------------------------------------
// Attribute check function
//
// Checks all attributes required for IPET calculation are attached
//
// - cache classification attributes (see naming conventions on
//   top of file)
//
// ----------------------------------------------------------------
bool CustomIPETAnalysis::CheckInputAttributes()
{
  Cfg *currentCfg;

  vector<Cfg *> lc = p->GetAllCfgs();
  for (unsigned int c = 0; c < lc.size(); c++)
  {
    currentCfg = lc[c];
    if (!CheckInputAttributesCacheClassification(currentCfg))
      return false;
    if (!CheckInputAttributesPipelineTiming(currentCfg))
      return false;
  }
  return true;
}

// ------------------------------------------------------------------
// Get an attribute value by name
// Shortcut to withdraw an NonSerialisableIntegerAttribute attached
// to a Cfg node
// ------------------------------------------------------------------
static long getIntegerAttribute(Node *n, string name)
{
  int val;
  assert(!isNULLPointer(n));
  //  if (!n->HasAttribute(name)) exit(-1);
  assert(n->HasAttribute(name));
  NonSerialisableIntegerAttribute ai = (NonSerialisableIntegerAttribute &)n->GetAttribute(name);
  val = ai.GetValue();
  return val;
}

// -----------------------------------------------------------------------
// Compute a static bound on the node n frequency on the given context
//
// Works in a context-dependent manner
//
// Tracks the call nodes of the given context.
// Then, for all nodes of interest (including n), compute the product of all
// their loops iteration bounds.
//
// -----------------------------------------------------------------------

unsigned int ComputeNodeFrequencyBound(Node *n, Context *context);

string CustomIPETAnalysis::mkVariableNameCustomSolver(string prefix, Node *n, string vcontext)
{
  return AnalysisHelper::mkVariableNameSolver(prefix, getIntegerAttribute(n, InternalAttributeId), vcontext);
}

string CustomIPETAnalysis::mkEdgeVariableNameCustomSolver(string prefix, Node *source, Node *target, string vcontext)
{
  return AnalysisHelper::mkEdgeVariableNameSolver(prefix, getIntegerAttribute(source, InternalAttributeId), getIntegerAttribute(target, InternalAttributeId), vcontext);
}

// ------------------------------------------------------------------
// Get the 4 deltas of an edge and put it in a integer vector,
// in the following order: FF, FN, NF, NN
// These deltas are generated by the PipelineAnalysis
// to be used with METHOD_CACHE_PIPELINE estimation method
// ------------------------------------------------------------------
vector<int> CustomIPETAnalysis::getDeltas(Edge &e, string context)
{
  vector<int> deltas;
  // ostringstream tmp;

  for (vector<string>::iterator it = deltaName.begin(); it != deltaName.end(); it++)
  {
    deltas.push_back(AnalysisHelper::getEdgeValueAttr(e, *it, context));
  }

  return deltas;
}

void CustomIPETAnalysis::AddILPVariable(string prefix, long source_id, long target_id, string vcontext, vector<string> &edgeName)
{
  string s = AnalysisHelper::mkEdgeVariableNameSolver(prefix, source_id, target_id, vcontext);
  edgeName.push_back(s);
}

// ------------------------------------------------------------------
// Generate the variable name used by the ILP for an edge
// in the following order: FF, FN, NF, NN
// to be used with METHOD_CACHE_PIPELINE estimation method
// ------------------------------------------------------------------
vector<string> CustomIPETAnalysis::generateEdgeVariableName(Edge &e, string vcontext)
{
  vector<string> edgeName;
  ostringstream tmp;

  Cfg *cfg = e.GetCfg();
  Node *source = cfg->GetSourceNode(&e);
  Node *target = cfg->GetTargetNode(&e);

  long source_id = getIntegerAttribute(source, InternalAttributeId);
  long target_id = getIntegerAttribute(target, InternalAttributeId);

  AddILPVariable("eff_", source_id, target_id, vcontext, edgeName);
  AddILPVariable("efn_", source_id, target_id, vcontext, edgeName);
  AddILPVariable("enf_", source_id, target_id, vcontext, edgeName);
  AddILPVariable("enn_", source_id, target_id, vcontext, edgeName);
  return edgeName;
}

/**  */
bool CustomIPETAnalysis::isReachableState(Context *c)
{
  if (isNULLPointer(c))
    return true;
  Cfg *vcfg = c->getCurrentFunction();
  if (vcfg->HasAttribute(ExternalWCETAttributeName))
    return false;
  return isReachableState(c->getCallerContext());
}

/*
  Generate call constraints for one CFG in Contextual IPET mode.
  For every call point of id X in context C1,
  if Y is the id of the first BB of callee
       (context C2=C1+"#"callnb+"#"calleeName), then
   the following constraint is generated:
    n_X_cC1 = n_Y_cC2
*/
void CustomIPETAnalysis::generateCallConstraints(ostringstream &os, Program *p)
{
  Cfg *vCFGCallee;
  vector<Cfg *> lcfg = p->GetAllCfgs();

  for (unsigned int callee = 0; callee < lcfg.size(); callee++)
  {
    // Generate call constraints for CFG callee
    vCFGCallee = lcfg[callee];

    if (!isDeadCode(vCFGCallee))
    {
      const ContextList &contexts = (ContextList &)vCFGCallee->GetAttribute(ContextListAttributeName);
      if (!vCFGCallee->HasAttribute(ExternalWCETAttributeName))
      {
        Node *start = vCFGCallee->GetStartNode();
        // Scan all contexts of callee
        // Get the list of execution contexts of the node
        unsigned int nc = contexts.size();
        for (unsigned int ic = 0; ic < nc; ic++)
        {
          Context *callee_context = contexts[ic];
          Node *n = callee_context->getCallerNode();
          if (!isNULLPointer(n))
          {
            if (isReachableState(callee_context))
            {
              Context *caller_context = callee_context->getCallerContext();
              vector<string> ncallers;
              string s1 = mkVariableNameCustomSolver("n_", n, caller_context->getStringId());
              ncallers.push_back(s1);
              string s2 = mkVariableNameCustomSolver("n_", start, callee_context->getStringId());
              // cout << " 6666 contrainst : " << s1 << " = " << s2 << endl;
              ncallers.push_back(s2);
              solver->generate_flow_constraint(os, ncallers);
            }
            else // A not reachable call (I hope)
            {
              string s2 = mkVariableNameCustomSolver("n_", start, callee_context->getStringId());
              solver->generate_null_variable(os, s2);
            }
          }
        }
      }
      else
      {
        if (!vCFGCallee->HasAttribute(InternalAttributeId))
          Logger::addFatal("Can't find the internal ID in external function " + vCFGCallee->getStringName());
        NonSerialisableIntegerAttribute &attr_id = (NonSerialisableIntegerAttribute &)vCFGCallee->GetAttribute(InternalAttributeId);
        int vcfg_id = attr_id.GetValue();

        unsigned int nc = contexts.size();
        for (unsigned int ic = 0; ic < nc; ic++)
        {
          Context *callee_context = contexts[ic];
          Node *n = callee_context->getCallerNode();
          Context *caller_context = callee_context->getCallerContext();
          vector<string> ncallers;
          string s = mkVariableNameCustomSolver("n_", n, caller_context->getStringId());
          ncallers.push_back(s);
          ncallers.push_back(AnalysisHelper::mkVariableNameSolver("ex_", vcfg_id, contexts[ic]->getStringId()));
          solver->generate_flow_constraint(os, ncallers);
        }
      }
    }
  }
}

void CustomIPETAnalysis::ComputeNodeExecutionTime_InstructionCacheLevel(Instruction *vinstr, Context *context, int numCache, int *wcet_first, int *wcet_next, bool *countFirst, bool *countNext)
{
  int currentAccessCost = levelAccessCostInstr[numCache];
  string classif = AnalysisHelper::getInstrStringAttr(vinstr, CodeCHMC[numCache], context);
  if (*countNext)
    (*wcet_next) = (*wcet_next) + currentAccessCost;
  if (*countFirst)
    (*wcet_first) = (*wcet_first) + currentAccessCost; // latence to not find the address in the current level

  if (classif == "AH")
  {
    *countFirst = false;
    *countNext = false;
  }
  else if ((classif == "AM") || (classif == "NC"))
  {
    if (numCache == NbICacheLevels)
    {
      if (*countFirst)
        (*wcet_first) = (*wcet_first) + MemoryLoadLatency;
      if (*countNext)
        (*wcet_next) = (*wcet_next) + MemoryLoadLatency;
    }
  }
  else if (classif == "FM")
  {
    if ((numCache == NbICacheLevels) && (*countFirst))
      (*wcet_first) = (*wcet_first) + MemoryLoadLatency;
    *countNext = false;
  }
}

// Count latencies (wcet_first, wcet_next when updateNext), according to the data access attributes ( never_accessed_data, always_accessed_data, occurrence_bound_data).
void CustomIPETAnalysis::DataCacheLevel_latency(int vCost, bool updateNext, int *wcet_first, int *wcet_next, bool never_accessed_data, bool always_accessed_data, unsigned int occurrence_bound_data)
{
  if (always_accessed_data)
  {
    (*wcet_first) = (*wcet_first) + vCost;
    if (updateNext)
      (*wcet_next) = (*wcet_next) + vCost;
  }
  else if (!never_accessed_data)
  {
    (*wcet_first) = (*wcet_first) + (vCost * occurrence_bound_data);
  }
}

// Update the different attributes for the next level
unsigned int CustomIPETAnalysis::DataCacheLevel_NextLevel(string classif, unsigned int memBlock, unsigned int frequency, bool *never_accessed_data, bool *always_accessed_data,
                                                          unsigned int occurrence_bound_data)
{
  // Next value of never_accessed_data
  if (classif == "AH")
    (*never_accessed_data) = true;

  // Next value of always_accessed_data
  if (*always_accessed_data)
    (*always_accessed_data) = ((classif == "AM") || (classif == "NC"));
  assert((classif != "AH") || (!(*always_accessed_data) && (*never_accessed_data)));

  // Next value of occurrence_bound_data
  if (*never_accessed_data)
    return 0;
  if (*always_accessed_data)
    return min(memBlock, frequency);
  if (classif == "FM")
    return min(memBlock, occurrence_bound_data);
  return occurrence_bound_data;
}

void CustomIPETAnalysis::ComputeNodeExecutionTime_DataCacheLevel(Instruction *vinstr, Context *context, int numCache, unsigned int frequency, int *wcet_first, int *wcet_next,
                                                                 bool *never_accessed_data, bool *always_accessed_data, unsigned int *occurrence_bound_data)
{
  string classif;
  unsigned int memBlock;

  classif = AnalysisHelper::getInstrStringAttr(vinstr, DataCHMC[numCache], context);
  if (classif != "AU")
  {
    assert(numCache != 1 || (*always_accessed_data));
    assert(!(*never_accessed_data));

    // Count latencies for a cache level (numCache)
    DataCacheLevel_latency(levelAccessCostData[numCache], true, wcet_first, wcet_next, *never_accessed_data, *always_accessed_data, *occurrence_bound_data);

    // Update the different attributes for the next level
    memBlock = AnalysisHelper::getInstrIntAttr(vinstr, blockCountName[numCache], context);
    (*occurrence_bound_data) = DataCacheLevel_NextLevel(classif, memBlock, frequency, never_accessed_data, always_accessed_data, *occurrence_bound_data);

    // Accesses to the main memory
    if (numCache == NbDCacheLevels)
    {
      DataCacheLevel_latency(MemoryLoadLatency, (classif == "AM" || classif == "NC"), wcet_first, wcet_next, *never_accessed_data, *always_accessed_data, *occurrence_bound_data);
    }
  }
  else
  {
    assert((numCache == 1) || (!(*always_accessed_data) && (*never_accessed_data)));
    (*always_accessed_data) = false;
    (*never_accessed_data) = true;
  }
}

bool CustomIPETAnalysis::ComputeInstrExecutionTime_NOPIPELINE_STORE_DCACHE(Instruction *vinstr, int *wcet_first, int *wcet_next)
{
  // This is introduce to not count for instance the next of an always miss
  // in the current cache level while in previous level it is a First miss
  if (Arch::isStore(vinstr->GetCode()))
  {
    *wcet_first = *wcet_first + MemoryStoreLatency;
    *wcet_next = *wcet_next + MemoryStoreLatency;
    return true;
  }
  return false;
}

void CustomIPETAnalysis::ComputeInstrExecutionTime_NOPIPELINE_ICACHE_PERFECTDCACHE(Instruction *vinstr, Context *context, int *wcet_first, int *wcet_next)
{
  int currentCache;
  bool countFirst, countNext;
  // This is introduce to not count for instance the next of an always miss
  // in the current cache level while in previous level it is a First miss
  *wcet_first = *wcet_first + PerfectDCacheLatency;
  *wcet_next = *wcet_next + PerfectDCacheLatency;

  // For each Cache.
  countFirst = true; // to know if we count the first access for the current cache level
  countNext = true;  // to know if we count the next access for the current cache level
  for (currentCache = 1; currentCache <= NbICacheLevels; currentCache++)
  {
    ComputeNodeExecutionTime_InstructionCacheLevel(vinstr, context, currentCache, wcet_first, wcet_next, &countFirst, &countNext);
  }
}

void CustomIPETAnalysis::ComputeInstrExecutionTime_NOPIPELINE_ICACHE_DCACHE(Instruction *vinstr, Context *context, unsigned int frequency, int *wcet_first, int *wcet_next)
{
  int currentCache;
  string vinstr_code;
  bool countFirst, countNext, always_accessed_data, never_accessed_data, bisLoad;
  unsigned int occurrence_bound_data;

  ComputeInstrExecutionTime_NOPIPELINE_STORE_DCACHE(vinstr, wcet_first, wcet_next);
  bisLoad = Arch::isLoad(vinstr->GetCode());

  countFirst = true; // to know if we count the first access for the current cache level
  countNext = true;  // to know if we count the next access for the current cache level
  always_accessed_data = true;
  never_accessed_data = false;
  occurrence_bound_data = 1;
  for (currentCache = 1; currentCache <= NbICacheLevels; currentCache++) // ok because NbICacheLevels = NbDCacheLevels
  {
    // Instruction cache
    ComputeNodeExecutionTime_InstructionCacheLevel(vinstr, context, currentCache, wcet_first, wcet_next, &countFirst, &countNext);
    // Data cache
    if (bisLoad)
      ComputeNodeExecutionTime_DataCacheLevel(vinstr, context, currentCache, frequency, wcet_first, wcet_next, &never_accessed_data, &always_accessed_data, &occurrence_bound_data);
  }
}

void CustomIPETAnalysis::ComputeInstrExecutionTime_NOPIPELINE_PERFECTICACHE_DCACHE(Instruction *vinstr, Context *context, unsigned int frequency, int *wcet_first, int *wcet_next)
{
  int currentCache;
  string vinstr_code;
  bool always_accessed_data, never_accessed_data;
  unsigned int occurrence_bound_data;

  // This is introduce to not count for instance the next of an always miss
  // in the current cache level while in previous level it is a First miss
  *wcet_first = *wcet_first + PerfectICacheLatency;
  *wcet_next = *wcet_next + PerfectICacheLatency;

  ComputeInstrExecutionTime_NOPIPELINE_STORE_DCACHE(vinstr, wcet_first, wcet_next);
  if (Arch::isLoad(vinstr->GetCode()))
  {
    always_accessed_data = true;
    never_accessed_data = false;
    occurrence_bound_data = 1;
    for (currentCache = 1; currentCache <= NbDCacheLevels; currentCache++)
    {
      ComputeNodeExecutionTime_DataCacheLevel(vinstr, context, currentCache, frequency, wcet_first, wcet_next, &never_accessed_data, &always_accessed_data, &occurrence_bound_data);
    }
  }
}

// -----------------------------------------------------------------------
// Compute the execution time of a block for its first and next iterations
//
// Works in both a context independent and context-dependent manner
// So far, this method considers the cache only
// (integration with pipeline analysis still to be done)
//
// Multiple levels of caches are considered (see Damiens's paper
// on WCET analysis for cache hierarchies for the details)
//
// Context is the execution context for which the BB execution time
// is computed ("" if the analysis is non-contextual)
// -----------------------------------------------------------------------
void CustomIPETAnalysis::ComputeNodeExecutionTime_NOPIPELINE_CACHE(Node *n, Context *context, int *pwcet_first, int *pwcet_next, bool perfectIcache, bool perfectDcache)
{
  Instruction *vinstr;
  int /*WCET_type*/ wcet_first, wcet_next;

  assert(!perfectIcache || !perfectDcache); // perfectIcache && perfectDcache are filtered before ComputeNodeExecutionTime_NOPIPELINE_NOCACHE
  wcet_first = 0;
  wcet_next = 0;

  vector<Instruction *> vi = n->GetInstructions();
  for (unsigned int inst = 0; inst < vi.size(); inst++)
  {
    vinstr = vi[inst];
    if (vinstr->IsCode())
    {
      if (perfectIcache)
      {
        if (!perfectDcache)
          ComputeInstrExecutionTime_NOPIPELINE_PERFECTICACHE_DCACHE(vinstr, context, ComputeNodeFrequencyBound(n, context), &wcet_first, &wcet_next);
        else
          ; // Does not occur.
      }
      else if (perfectDcache)
        ComputeInstrExecutionTime_NOPIPELINE_ICACHE_PERFECTDCACHE(vinstr, context, &wcet_first, &wcet_next);
      else
        ComputeInstrExecutionTime_NOPIPELINE_ICACHE_DCACHE(vinstr, context, ComputeNodeFrequencyBound(n, context), &wcet_first, &wcet_next);
    }
  }
  *pwcet_first = wcet_first;
  *pwcet_next = wcet_next;
}

// ------------------------------------------------
//
// Generate Node Ids for one CFG
// --------------------------------
//
// Generate a unique Id to each node of a
// and add the node Id as an internal attribute
// attached to the node
//
// NB: this step was separated from constraint
// generation to allow edges between nodes
// of different Cfgs
//
// ------------------------------------------------
bool CustomIPETAnalysis::generateNodeIds(ostringstream &os, Cfg *c)
{
  // Used to generate BB numbers
  // NB: should be static, because BB numbers should be unique for all Cfgs
  static int bb_id = 0;

  if (c->HasAttribute(ExternalWCETAttributeName))
  {
    // Attribute an id
    NonSerialisableIntegerAttribute attr_id(bb_id);
    c->SetAttribute(InternalAttributeId, attr_id);
    bb_id++;
    return true;
  }

  vector<Node *> vn = c->GetAllNodes();

  const ContextList &contexts = (ContextList &)c->GetAttribute(ContextListAttributeName);
  unsigned int nc = contexts.size();

  // Assign a unique number to every BB (used in constraint generation)
  // ------------------------------------------------------------------
  for (unsigned int i = 0; i < vn.size(); i++)
  {
    // Attribute an id
    NonSerialisableIntegerAttribute attr_id(bb_id);
    vn[i]->SetAttribute(InternalAttributeId, attr_id);

    // Store correspondance between id of variable in ILP system and node pointer
    for (unsigned int ic = 0; ic < nc; ic++)
    {
      string idVar = AnalysisHelper::mkVariableNameSolver("n_", bb_id, contexts[ic]->getStringId());
      node_ids[idVar] = vn[i];
    }
    bb_id++;
  }
  return true;
}

/*
  Set the attributes InternalAttributeWCETfirst and InternalAttributeWCETnext to each node of vn in  "contextual" mode.
  For all execution contexts (once only if the analysis is not contextual).
 */
void CustomIPETAnalysis::ComputeNodesExecutionTime_NOPIPELINE_CACHE(vector<Node *> vn, const ContextList &contexts, bool perfectIcache, bool perfectDcache)
{
  int wcet_first, wcet_next;
  string contextName;
  unsigned int i, ic, nc;

  nc = contexts.size();
  for (i = 0; i < vn.size(); i++)
  {
    // For all execution contexts (once only if the analysis is not contextual).
    // Compute the node execution time
    Node *n = vn[i];
    // To generate non contextual information once only
    for (ic = 0; ic < nc; ic++)
    {
      wcet_first = 0;
      wcet_next = 0;
      ComputeNodeExecutionTime_NOPIPELINE_CACHE(n, contexts[ic], &wcet_first, &wcet_next, perfectIcache, perfectDcache);

      // Attach the execution time as an attribute to the node
      contextName = contexts[ic]->getStringId();
      NonSerialisableIntegerAttribute attr_first(wcet_first);
      n->SetAttribute(AnalysisHelper::mkContextAttrName(InternalAttributeWCETfirst, contextName), attr_first);
      NonSerialisableIntegerAttribute attr_next(wcet_next);
      n->SetAttribute(AnalysisHelper::mkContextAttrName(InternalAttributeWCETnext, contextName), attr_next);
    }
  }
}

void CustomIPETAnalysis::generateConstraints_NOPIPELINE_CACHE(vector<Node *> vn, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  string contextName;
  unsigned int i, ic, nc;

  // Here, there is a variable in the ILP system per basic block,
  // first and next executions and execution
  // context, plus one variable per edge and execution context.
  // Execution contexts are coded as integers (see naming conventions on top)
  // In the case non-contextual IPET is used, context number 0 is used
  nc = contexts.size();
  for (i = 0; i < vn.size(); i++)
  {
    Node *n = vn[i];
    // Get the list of execution contexts of the node
    for (ic = 0; ic < nc; ic++)
    {
      contextName = contexts[ic]->getStringId();
      // Get value of wcets for first and next iters
      vid.push_back(mkVariableNameCustomSolver("nf_", n, contextName));
      int wcet_first = getIntegerAttribute(n, AnalysisHelper::mkContextAttrName(InternalAttributeWCETfirst, contextName));
      vwcet.push_back(wcet_first);

      vid.push_back(mkVariableNameCustomSolver("nn_", n, contextName));
      int wcet_next = getIntegerAttribute(n, AnalysisHelper::mkContextAttrName(InternalAttributeWCETnext, contextName));
      vwcet.push_back(wcet_next);
    }
  }
}

/*
   Generate WCETs for all nodes (here, simple sum of number of instructions)
 */
void CustomIPETAnalysis::ComputeNodesExecutionTime_NOPIPELINE_NOCACHE(vector<Node *> vn, const ContextList &contexts)
{
  Node *n;
  int wcet;
  string contextName;
  unsigned int nc, ic, i, inst;

  nc = contexts.size();

  for (i = 0; i < vn.size(); i++)
  {
    wcet = 0;
    n = vn[i];
    vector<Instruction *> vi = n->GetInstructions();
    for (inst = 0; inst < vi.size(); inst++)
    {
      if (vi[inst]->IsCode())
      {
        wcet = wcet + PerfectICacheLatency;
      }
    }
    if (n->HasAttribute("WCET"))
    {
      auto attribute = (SerialisableUnsignedLongAttribute &)n->GetAttribute("WCET");
      wcet = attribute.GetValue();
    }

    NonSerialisableIntegerAttribute attr_wcet(wcet);
    for (ic = 0; ic < nc; ic++)
    {
      contextName = contexts[ic]->getStringId();
      string attr = AnalysisHelper::mkContextAttrName(InternalAttributeWCETfirst, contextName);
      n->SetAttribute(attr, attr_wcet);
    }
  }
}

void CustomIPETAnalysis::generateConstraints_NOPIPELINE_NOCACHE(vector<Node *> vn, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  Node *n;
  int wcet;
  string contextName;
  unsigned int nc, ic, i;

  nc = contexts.size();
  for (i = 0; i < vn.size(); i++)
  {
    n = vn[i];
    for (ic = 0; ic < nc; ic++)
    {
      Context *callee_context = contexts[ic];
      contextName = callee_context->getStringId();
      // Get value of wcets for first iters
      wcet = getIntegerAttribute(n, AnalysisHelper::mkContextAttrName(InternalAttributeWCETfirst, contextName));
      string s = mkVariableNameCustomSolver("n_", n, contextName);
      vid.push_back(s);
      vwcet.push_back(wcet);
      // cout << " NOPIPELINE_NOCACHE = [ " << s << ',' << wcet << "]" << endl;
    }
  }
}

void CustomIPETAnalysis::generateConstraints_PIPELINE_CACHE(Cfg *c, vector<Node *> vn, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  vector<Edge *> ve = c->GetAllEdges();
  Node *n;
  string contextName;
  unsigned int ic, nc;

  // Here, are generated variables for the ILP system per basic block :
  // first and next executions frequencies per context (vid vector). These variables
  // are associated with wcet values (vwcet vector).
  // Execution contexts are coded as integers (see naming conventions on top)
  nc = contexts.size();
  for (std::vector<Node *>::iterator it = vn.begin(); it != vn.end(); it++)
  {
    // Get the list of execution contexts of the node
    for (ic = 0; ic < nc; ic++)
    {
      n = (*it);
      contextName = contexts[ic]->getStringId();
      // get value of wcet for first iteration
      string idVar = mkVariableNameCustomSolver("nf_", n, contextName);
      vid.push_back(idVar);

      // call and return deltas associated with node frequency
      if (n->IsCall())
      {
        vid.push_back(idVar); // for the call
        vid.push_back(idVar); // for the return
      }

      vwcet.push_back(AnalysisHelper::getNodeValueAttr(n, NodeExecTimeFirstAttributeName, contextName));
      if (n->IsCall())
      {
        vwcet.push_back(AnalysisHelper::getNodeValueAttr(n, CallDeltaFirstAttributeName, contextName));
        vwcet.push_back(AnalysisHelper::getNodeValueAttr(n, ReturnDeltaFirstAttributeName, contextName));
      }

      // get value of wcet for next iteration
      idVar = mkVariableNameCustomSolver("nn_", n, contextName);
      vid.push_back(idVar);

      // call and return deltas associated with node frequency
      if (n->IsCall())
      {
        vid.push_back(idVar); // for the call
        vid.push_back(idVar); // for the return
      }

      vwcet.push_back(AnalysisHelper::getNodeValueAttr(n, NodeExecTimeNextAttributeName, contextName));
      if ((*it)->IsCall())
      {
        vwcet.push_back(AnalysisHelper::getNodeValueAttr(n, CallDeltaNextAttributeName, contextName));
        vwcet.push_back(AnalysisHelper::getNodeValueAttr(n, ReturnDeltaNextAttributeName, contextName));
      }
    }
  }

  // Here, are generated variables for the ILP system per edges(for the objective function).
  for (std::vector<Edge *>::iterator it = ve.begin(); it != ve.end(); it++)
  {
    for (ic = 0; ic < nc; ic++)
    {
      if (!(c->GetSourceNode(*it))->IsCall())
      {
        contextName = contexts[ic]->getStringId();
        vector<int> deltas = getDeltas(*(*it), contextName);
        vector<string> variableName = generateEdgeVariableName(*(*it), contextName);

        for (unsigned int i = 0; i < deltas.size(); i++)
        {
          vid.push_back(variableName[i]);
          vwcet.push_back(deltas[i]);
        }
      }
    }
  }
}

// called for the methods METHOD_NOPIPELINE_ICACHE_DCACHE, METHOD_NOPIPELINE_PERFECTICACHE_DCACHE,METHOD_NOPIPELINE_ICACHE_PERFECTDCACHE,
//  METHOD_PIPELINE_ICACHE_DCACHE and METHOD_PIPELINE_ICACHE_PERFECTDCACHE  (when the CACHE_ANALYSIS is done)
void CustomIPETAnalysis::generateConstraints_inside_CACHE_BB(ostringstream &os, vector<Node *> vn, const ContextList &contexts)
{
  Node *n;
  string contextName;

  unsigned int nc = contexts.size();
  for (unsigned int i = 0; i < vn.size(); i++)
  {
    n = vn[i];
    // BB frequency = freq first + freq next for all contexts
    // Get the list of execution contexts of the node
    for (unsigned int ic = 0; ic < nc; ic++)
    {
      contextName = contexts[ic]->getStringId();
      vector<string> vs;
      vs.push_back(mkVariableNameCustomSolver("n_", n, contextName));
      vs.push_back(mkVariableNameCustomSolver("nf_", n, contextName));
      vs.push_back(mkVariableNameCustomSolver("nn_", n, contextName));
      solver->generate_flow_constraint(os, vs);

      // Freq first <=1 (bound)
      vector<string> vsf;
      vsf.push_back(mkVariableNameCustomSolver("nf_", n, contextName));
      solver->generate_inequality(os, vsf, 1);
    }
  }
}

// Generate flow constraints for every edges ( restricted to METHOD_CACHE_PIPELINE )
void CustomIPETAnalysis::generateConstraints_PIPELINE_CACHE_edges(ostringstream &os, vector<Edge *> ve, const ContextList &contexts)
{
  string contextName;
  unsigned int nc = contexts.size();

  for (vector<Edge *>::iterator it = ve.begin(); it != ve.end(); it++)
  {
    for (unsigned int ic = 0; ic < nc; ic++)
    {
      vector<string> vs;

      Cfg *cfg = (*it)->GetCfg();
      Node *source = cfg->GetSourceNode((*it));
      Node *target = cfg->GetTargetNode((*it));
      contextName = contexts[ic]->getStringId();
      vs.push_back(mkEdgeVariableNameCustomSolver("e_", source, target, contextName));

      vector<string> edgeVariable = generateEdgeVariableName(*(*it), contextName);
      vs.insert(vs.end(), edgeVariable.begin(), edgeVariable.end());

      // Fedge = Fff + Ffn + Fnf + Fnn
      solver->generate_flow_constraint(os, vs);

      // Fff + Ffn <= 1
      vector<string> vtmp;
      vtmp.push_back(edgeVariable[0]);
      vtmp.push_back(edgeVariable[1]);
      solver->generate_inequality(os, vtmp, 1);

      // Fff + Fnf <= 1
      vtmp.clear();
      vtmp.push_back(edgeVariable[0]);
      vtmp.push_back(edgeVariable[2]);
      solver->generate_inequality(os, vtmp, 1);
    }
  }
}

// Generate a constraint for every BB / edge for every execution context  ( no restriction on current method )
void CustomIPETAnalysis::generateConstraints_BB_edge_eachContext(Cfg *c, ostringstream &os, vector<Node *> vn, vector<Edge *> ve, const ContextList &contexts)
{
  string contextName;
  unsigned int nc = contexts.size();

  for (unsigned int i = 0; i < vn.size(); i++)
  {
    Node *n = vn[i];
    for (unsigned int ic = 0; ic < nc; ic++)
    {
      Context *vcontext = contexts[ic];
      contextName = vcontext->getStringId();

      // Constraint generation (flow constraints)
      // ----------------------------------------
      // Incoming edges
      {
        vector<Edge *> in_edges = c->GetIncomingEdges(n);
        vector<string> vs;
        string snode1 = mkVariableNameCustomSolver("n_", n, contextName);
        vs.push_back(snode1);

        for (unsigned int ei = 0; ei < in_edges.size(); ei++)
        {
          Edge *edge_i = in_edges[ei];
          // If the edge is deactivated, we do not generate a constraint for it
          if (edge_i->HasAttribute(EdgeDeactivated))
          {
            continue;
          }
          Node *source_i = c->GetSourceNode(edge_i);
          string s = mkEdgeVariableNameCustomSolver("e_", source_i, n, contextName);

          vs.push_back(s);
        }

        // For the analysis entry point, we must generate a fake incoming edge, if the node
        // is in the middle of the function
        if (n->HasAttribute(AnalysisStartingPointAttribute) && c->GetIncomingEdges(n).size() > 0)
        {
          vs.push_back(mkVariableNameCustomSolver("n_entry_", n, contextName));
        }
        // Add an imaginary edge from the exit of function `f` to
        // the current node, if the previous node is a call to `f`
        for (Node *pred : c->GetPredecessors(n))
        {
          if (pred->IsCall())
          {
            // If the predecessor call "checkpoint", then
            // we can ignore it
            if (AnalysisHelper::isCheckpointCall(pred))
              continue;
            Cfg *cfg = pred->GetCallee();
            assert(cfg != 0);
            ostringstream oss;
            Context *callee_context = vcontext->getCalleeContext(pred);
            assert(callee_context != nullptr);
            oss << "e_" << cfg->GetName().back() << "_c" << callee_context->getStringId() << "_";
            oss << mkVariableNameCustomSolver("n_", n, contextName);
            vs.push_back(oss.str());
          }
        }
        // If there are more than one incoming edge, we generate a constraint
        if (vs.size() > 1)
        {
          solver->generate_flow_constraint(os, vs);
        } // If the node is unreachable, deactivate it (except if it is a function starting point)
        else if (!(c->GetStartNode() == n && !c->HasAttribute(FunctionDeactivated)))
        {
          solver->generate_equality(os, vs, 0);
        }
      }

      // Outgoing edges
      {
        vector<Edge *> out_edges = c->GetOutgoingEdges(n);
        vector<string> vs;
        string snode2 = mkVariableNameCustomSolver("n_", n, contextName);
        vs.push_back(snode2);

        for (unsigned int ei = 0; ei < out_edges.size(); ei++)
        {
          Edge *edge_i = out_edges[ei];
          if (edge_i->HasAttribute(EdgeDeactivated))
          {
            continue;
          }
          Node *dest_i = c->GetTargetNode(edge_i);
          string s = mkEdgeVariableNameCustomSolver("e_", n, dest_i, contextName);
          vs.push_back(s);
        }

        solver->generate_flow_constraint(os, vs);
      }
    }
  }
}

/**
 * @brief Return the context of the entry point function that are reachable
 * from the current context
 *
 * @param context the starting context
 * @return vector<Context*> the list of reachable contexts that contain entry points
 */
vector<Context *> CustomIPETAnalysis::entryPointContextsFromCtx(Context *context)
{
  // If the current context call from the entry point, we can already
  // reach the context (as we are in this context)
  if (p->GetEntryPoint() == context->getCurrentFunction())
  {
    vector<Context *> contexts;
    contexts.push_back(context);
    return contexts;
  }

  // Else, we explore the reachable contexts
  vector<Context *> res;
  for (Context *c : context->getSuccessors())
  {
    for (Context *entry_ctxs : entryPointContextsFromCtx(c))
    {
      res.push_back(entry_ctxs);
    }
  }

  return res;
}

/**
    Constraint for back edges of loops:
    Generates for every loop node contraints of the form: f_node <= maxiter * sum(entry_edges)

    Nodes belonging to subloops should not be considered,
    as well as the loop head (except if it is the only node in the loop)
  */
void CustomIPETAnalysis::generateConstraints_back_edges_loops(Cfg *c, ostringstream &os, vector<Node *> vn, const ContextList &contexts)
{
  string contextName;
  bool resume_from_loop;
  Node *resume_from;
  vector<Loop *> vl = c->GetAllLoops();
  unsigned int nc = contexts.size();

  // For all loops
  for (unsigned int l = 0; l < vl.size(); l++)
  {
    Loop *CurrentLoop = vl[l];
    // For all execution contexts
    for (unsigned int ic = 0; ic < nc; ic++)
    {
      resume_from_loop = false;
      contextName = contexts[ic]->getStringId();

      SerialisableIntegerAttribute bound = (SerialisableIntegerAttribute &)CurrentLoop->GetAttribute(MaxiterAttributeName);
      long maxiter = bound.GetValue();
      vector<string> vs;
      VECTOR_WCET vcst;
      // Scan the incoming edges of the loop head, to add natural constraint to enter a loop
      Node *head = CurrentLoop->GetHead();
      vector<Edge *> ie = c->GetIncomingEdges(head);
      for (unsigned int e = 0; e < ie.size(); e++)
      {
        Node *origin = c->GetSourceNode(ie[e]);
        // If the edge is deactivated, cannot enter the loop from this edge, do not generate a constraint
        if (ie[e]->HasAttribute(EdgeDeactivated))
        {
          continue;
        }

        if (!CurrentLoop->FindInLoop(origin))
        {
          Node *destination = c->GetTargetNode(ie[e]);
          string s = mkEdgeVariableNameCustomSolver("e_", origin, destination, contextName);
          vs.push_back(s);
          long constant = maxiter * (-1);
          vcst.push_back(constant);
        }
      }

      // If we may resume from inside the loop, we need to add the incoming edges
      for (Context *context : contexts[ic]->getSuccessors())
      {
        // c is the context of a function called by the current function
        // We need to check that the function called is called in the loop
        // If so, we check is the function called calls a checkpoint at one point
        // (either directly or indirectly, through other function calls)

        // 1. Check if the function call  is in the loop
        // If the caller node is not in the loop, we won't add incoming edge to the loop
        // constraint
        Node *call_node = context->getCallerNode();
        bool inloop = false;
        for (Node *n : CurrentLoop->GetAllNodes())
        {
          if (n == call_node)
          {
            inloop = true;
            break;
          }
        }
        if (!inloop)
        {
          continue;
        }

        // 2. Check if we might resume program execution from the function
        // (i.e. if the callee, or a function called by callee, contains the entry point)
        //
        // Note that cases where the entrypoint is in the loop is not handled here, but below
        //
        // If we might resume from the callee, we need to allow to resume execution of the loop
        // adding a new entry point to the loop, called "resume point"
        //
        // This is added with a new parameter for the loop bound constraint
        // -max_iter*loop_entry_point + BB_loop <= 0
        // becomes
        // -max_iter*loop_entry_point - max_iter*resume_point + BB_loop <= 0
        for (Context *entry_pt_ctx : entryPointContextsFromCtx(context))
        {
          assert(entry_pt_ctx != nullptr);
          // Debug printing in the ILP file
          vector<Context *> path;
          string path_str = "";
          path.push_back(entry_pt_ctx);
          path_str += entry_pt_ctx->getCurrentFunction()->GetName().back() + " -> ";
          while (path.back()->getCallerContext() != context && path.back()->getCallerContext() != nullptr)
          {
            path.push_back(path.back()->getCallerContext());
            path_str += path.back()->getCurrentFunction()->GetName().back() + " -> ";
          }
          os << "\\ Adding new constraint to enable resuming from " << path_str << endl;

          // Add the entry point edge to the loop constraints
          // If this edge is taken, it means that the program resumes from this entry
          // point, and thus from the loop (which might have maxiter iteration left).
          long constant = maxiter * (-1);

          // Find the entry point node
          Node *starting_node = nullptr;
          for (Node *n : entry_pt_ctx->getCurrentFunction()->GetAllNodes())
          {
            if (n->HasAttribute(AnalysisStartingPointAttribute))
            {
              starting_node = n;
              break;
            }
          }
          assert(starting_node != nullptr && "The entry point function has no starting nodes, which shouldn't happen");

          // Add the entry point edge to the loop constraints
          string resumed_from = mkVariableNameCustomSolver("n_entry_", starting_node, entry_pt_ctx->getStringId());
          vs.push_back(resumed_from);
          vcst.push_back(constant);
          // We might resume the loop from this node
          Node *resume_call = context->getCallerNode();
          if (c->GetSuccessors(resume_call).size() > 0)
          {
            assert(c->GetSuccessors(resume_call).size() == 1 && "A function can only have one successor");
            resume_from = c->GetSuccessors(resume_call)[0];
            resume_from_loop = true;
          }
        }
      }

      // TODO : Est ce que l'on ne pourrait pas direct regarder parmis
      // les noeuds de la boucle si il y a le starting point ???
      bool checkpoint_in_loop = false;
      // Check if there is a checkpoint in the loop
      for (Node *bb : CurrentLoop->GetAllNodes())
      {
        if (AnalysisHelper::isCheckpointCall(bb))
        {
          checkpoint_in_loop = true;
          break;
        }
      }

      if (checkpoint_in_loop)
      {
        // Add the entry point edge to the loop constraints
        // If this edge is taken, it means that the program resumes from this entry
        // point, and thus from the loop (which might have maxiter iteration left).
        long constant = maxiter * (-1);

        // Find the entry point node
        Node *starting_node = nullptr;
        for (Node *n : CurrentLoop->GetAllNodes())
        {
          if (n->HasAttribute(AnalysisStartingPointAttribute))
          {
            starting_node = n;
            break;
          }
        }

        if (starting_node != nullptr)
        {
          // Add the entry point edge to the loop constraints
          string resumed_from = mkVariableNameCustomSolver("n_entry_", starting_node, contextName);
          vs.push_back(resumed_from);
          vcst.push_back(constant);
          resume_from = starting_node;
          resume_from_loop = true;
        }
      }

      // Since we might resume from the loop, we want to know which basic blocks are
      // after the checkpoint, and thus might execute maxiter times, and those
      // that are before the checkpoint, and thus might execute maxiter-1 times
      set<Node *> nodes_between_start_and_loop_head = set<Node *>();
      set<Node *> visited = set<Node *>();
      queue<Node *> to_visit = queue<Node *>();
      if (resume_from_loop)
      {
        if (resume_from == nullptr)
        {
          Logger::addError("[Checkpoint Isolation] Error while trying to determine the maxiter for nodes before a checkoint");
          Logger::addError("[Checkpoint Isolation] Defaulting to max iterations");
        }
        else
        {
          to_visit.push(resume_from);
        }

        // Traverse the graph to find all nodes between the starting point and the loop head
        while (!to_visit.empty())
        {
          Node *current = to_visit.front();
          to_visit.pop();
          if (visited.find(current) != visited.end())
          {
            continue;
          }
          visited.insert(current);

          // If the current node is out of the loop, stop the traversal here
          // If it is the head of the loop, stop the traversal here
          if (!CurrentLoop->FindInLoop(current) || current == head)
          {
            continue;
          }

          // If the current node reach this point, it means that it is
          // - In the loop
          // - Between the entry point and the loop head
          // Does it cover cases where the exit node is not in the loop head ?
          nodes_between_start_and_loop_head.insert(current);
          // Add the successors to the queue
          for (Node *succ : c->GetSuccessors(current))
          {
            to_visit.push(succ);
          }
        }
      }

      // Generate, for each node of the loop, a constraint of the form
      // -maxiter * loop_entry_point_1 - maxiter * loop_entry_point_2 - ... + BB_loop <= 0
      vs.push_back("");
      vcst.push_back(0L);
      vector<Node *> vn = CurrentLoop->GetAllNodesNotNested();
      for (unsigned int n = 0; n < vn.size(); n++)
      {
        Node *node = vn[n];
        // Modified by LBesnard May 2019: to solve a bug on multi-BB test ( example: for (int i=0 ; i <=n && i < 30; i++)... )
        if (BBLoopMustBeConstrained(c, node, head, CurrentLoop) || (node == head && vn.size() == 1))
        {
          string s = mkVariableNameCustomSolver("n_", node, contextName);
          vs[vs.size() - 1] = s;
          vcst[vcst.size() - 1] = 1L;

          if (resume_from_loop && nodes_between_start_and_loop_head.find(node) == nodes_between_start_and_loop_head.end())
          {
            // If we resume from the loop and the node is not between the starting point and the loop head,
            // it means that it can do at most maxiter-1 iterations
            // We need to diminish it's maxiter bound by one
            vcst[vcst.size() - 2] += 1;
          }

          if (vs.size() > 1)
          {
            os << "\\ % Loop constraint for loop in " << c->GetName().back() << " (context " << contextName << ")" << endl;
            solver->generate_linear_inequality(os, vs, vcst, 0);
          }

          if (resume_from_loop && nodes_between_start_and_loop_head.find(node) == nodes_between_start_and_loop_head.end())
          {
            // Set the max-iter back to it's previous value
            vcst[vcst.size() - 2] -= 1;
          }
        }
      }
    }
  }
}

/**
   @return true if a Basic block must be contraint, false otherwise.
   For a basic block (node), element of a loop (loop) with head its entry node.
 */
bool CustomIPETAnalysis::BBLoopMustBeConstrained(Cfg *c, Node *node, Node *head, Loop *loop)
{
  // Added by LBesnard. return ( node != head); replaced by

  if (node == head)
    return false;
  vector<Node *> succs = c->GetSuccessors(node);

  for (unsigned int i = 0; i < succs.size(); i++)
    if (succs[i] == head)
      return true; // do {  } while ();

  for (unsigned int i = 0; i < succs.size(); i++)
    if (!loop->FindInLoop(succs[i]))
      return false;
  return true;
}

vector<Node *> CustomIPETAnalysis::IsolatedNopNode(Cfg *c)
{
  vector<Node *> vn, vn_ori;
  Node *CurrentNode;

  vn_ori = c->GetAllNodes();
  for (unsigned int j = 0; j < vn_ori.size(); j++)
  {
    CurrentNode = vn_ori[j];
    if (!CurrentNode->isIsolatedNopNode()) // filtering nop ARM
      vn.push_back(CurrentNode);
  }
  return vn;
}

void CustomIPETAnalysis::generateConstraints_PIPELINE_ICACHE_DCACHE(ostringstream &os, Cfg *c, vector<Node *> &vn, vector<Edge *> &ve, const ContextList &contexts, vector<string> &vid,
                                                                    VECTOR_WCET &vwcet)
{
  // TODO : Ajouter un warning car DCACHE non pris en compte dans analyse pipeline!
  generateConstraints_PIPELINE_CACHE(c, vn, contexts, vid, vwcet);
  generateConstraints_inside_CACHE_BB(os, vn, contexts);
  generateConstraints_PIPELINE_CACHE_edges(os, ve, contexts); // Generate flow constraints inside the BB
}

void CustomIPETAnalysis::generateConstraints_PIPELINE_ICACHE_PERFECTDCACHE(ostringstream &os, Cfg *c, vector<Node *> &vn, vector<Edge *> &ve, const ContextList &contexts, vector<string> &vid,
                                                                           VECTOR_WCET &vwcet)
{
  // to be modified : Perfect DataCache
  generateConstraints_PIPELINE_ICACHE_DCACHE(os, c, vn, ve, contexts, vid, vwcet);
}

void CustomIPETAnalysis::generateConstraints_NOPIPELINE_ICACHE_DCACHE(ostringstream &os, Cfg *c, vector<Node *> &vn, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  ComputeNodesExecutionTime_NOPIPELINE_CACHE(vn, contexts, false, false);
  generateConstraints_NOPIPELINE_CACHE(vn, contexts, vid, vwcet);
  generateConstraints_inside_CACHE_BB(os, vn, contexts);
}

void CustomIPETAnalysis::generateConstraints_NOPIPELINE_ICACHE_PERFECTDCACHE(ostringstream &os, Cfg *c, vector<Node *> &vn, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  ComputeNodesExecutionTime_NOPIPELINE_CACHE(vn, contexts, false, true);
  generateConstraints_NOPIPELINE_CACHE(vn, contexts, vid, vwcet);
  generateConstraints_inside_CACHE_BB(os, vn, contexts);
}

void CustomIPETAnalysis::generateConstraints_NOPIPELINE_PERFECTICACHE_DCACHE(ostringstream &os, Cfg *c, vector<Node *> &vn, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  ComputeNodesExecutionTime_NOPIPELINE_CACHE(vn, contexts, true, false);
  generateConstraints_NOPIPELINE_CACHE(vn, contexts, vid, vwcet);
  generateConstraints_inside_CACHE_BB(os, vn, contexts);
}

void CustomIPETAnalysis::generateConstraints_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE(ostringstream &os, Cfg *c, vector<Node *> &vn, const ContextList &contexts, vector<string> &vid,
                                                                                    VECTOR_WCET &vwcet)
{
  ComputeNodesExecutionTime_NOPIPELINE_NOCACHE(vn, contexts);
  generateConstraints_NOPIPELINE_NOCACHE(vn, contexts, vid, vwcet);
}

/* Fill-in vid and WCET to be able to generate the objective function
   This part is dependent on the type of IPET method selected,
   which fixes the naming convention of variables
*/
void CustomIPETAnalysis::generateConstraints_IPET_selected_method(ostringstream &os, Cfg *c, vector<Node *> &vn, vector<Edge *> &ve, const ContextList &contexts, vector<string> &vid,
                                                                  VECTOR_WCET &vwcet)
{
  switch (method)
  {
  case METHOD_PIPELINE_ICACHE_DCACHE:
    generateConstraints_PIPELINE_ICACHE_DCACHE(os, c, vn, ve, contexts, vid, vwcet);
    break;

  case METHOD_PIPELINE_ICACHE_PERFECTDCACHE:
    generateConstraints_PIPELINE_ICACHE_PERFECTDCACHE(os, c, vn, ve, contexts, vid, vwcet);
    break;

  case METHOD_NOPIPELINE_ICACHE_DCACHE:
    generateConstraints_NOPIPELINE_ICACHE_DCACHE(os, c, vn, contexts, vid, vwcet);
    break;

  case METHOD_NOPIPELINE_ICACHE_PERFECTDCACHE:
    generateConstraints_NOPIPELINE_ICACHE_PERFECTDCACHE(os, c, vn, contexts, vid, vwcet);
    break;

  case METHOD_NOPIPELINE_PERFECTICACHE_DCACHE:
    generateConstraints_NOPIPELINE_PERFECTICACHE_DCACHE(os, c, vn, contexts, vid, vwcet);
    break;

  case METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE:
    generateConstraints_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE(os, c, vn, contexts, vid, vwcet);
    break;

  default:;
  }
}

void CustomIPETAnalysis::printIPETCommand()
{
  string str_method;
  switch (method)
  {
  case METHOD_PIPELINE_ICACHE_DCACHE:
    str_method = "PIPELINE_ICACHE_DCACHE";
    break;

  case METHOD_PIPELINE_ICACHE_PERFECTDCACHE:
    str_method = "PIPELINE_ICACHE_PERFECTDCACHE";
    break;

  case METHOD_NOPIPELINE_ICACHE_DCACHE:
    str_method = "NOPIPELINE_ICACHE_DCACHE";
    break;

  case METHOD_NOPIPELINE_ICACHE_PERFECTDCACHE:
    str_method = "NOPIPELINE_ICACHE_PERFECTDCACHE";
    break;

  case METHOD_NOPIPELINE_PERFECTICACHE_DCACHE:
    str_method = "NOPIPELINE_PERFECTICACHE_DCACHE";
    break;

  case METHOD_NOPIPELINE_PERFECTICACHE_PERFECTDCACHE:
    str_method = "NOPIPELINE_PERFECTICACHE_PERFECTDCACHE";
    break;

  default:
    str_method = "UNKNOWN";
  }
  Logger::print("IPET method: " + str_method);
}

void CustomIPETAnalysis::generateConstraints_ExternalFunction(Cfg *c, const ContextList &contexts, vector<string> &vid, VECTOR_WCET &vwcet)
{
  if (c->HasAttribute(ExternalWCETAttributeName))
  {
    if (!c->HasAttribute(InternalAttributeId))
      Logger::addFatal("Can't find the external ID in external function " + c->getStringName());

    ExternalWCETAttributeNameType &ia = (ExternalWCETAttributeNameType &)c->GetAttribute(ExternalWCETAttributeName);
    WCET_type wcet = ia.GetValue();

    NonSerialisableIntegerAttribute &attr_id = (NonSerialisableIntegerAttribute &)c->GetAttribute(InternalAttributeId);
    int vcfg_id = attr_id.GetValue();

    for (int ic = contexts.size() - 1; ic >= 0; --ic)
    {
      Context *callee_context = contexts[ic];
      Node *n = callee_context->getCallerNode();
      if (!isNULLPointer(n) && !n->GetCfg()->HasAttribute(ExternalWCETAttributeName))
      {
        string s = AnalysisHelper::mkVariableNameSolver("ex_", vcfg_id, contexts[ic]->getStringId());
        vid.push_back(s);
        vwcet.push_back(wcet);
      }
    }
  }
}

// ------------------------------------------------
//
// Generate constraints for one CFG
// --------------------------------
//
// Generates all structural constraints for one Cfg
//
// - os is the stream where constraints should be generated
// - c is the Cfg for which constraints have to be generated
// - vid is the vector of generated variables in the ILP system
// - vwcet is the vector of constants in the ILP system
// (these two last parameters are subsequently used to
// generate the objective function)
//
// Constraint generation is not directly done in this method.
// A generic interface for constraint generation is
// provided in CustomSolver.h and implemented for two solvers
// (lp_solve and CPLEX)
//
// Assumes each node has a node Id (done by function
// generateNodeIds)
//
// ------------------------------------------------

bool CustomIPETAnalysis::generateConstraints(ostringstream &os, Cfg *c, vector<string> &vid, VECTOR_WCET &vwcet)
{
  vector<Node *> vn;
  vector<Edge *> ve = c->GetAllEdges();

  vn = IsolatedNopNode(c);
  const ContextList &contexts = (ContextList &)c->GetAttribute(ContextListAttributeName);
  if (c->HasAttribute(ExternalWCETAttributeName))
    generateConstraints_ExternalFunction(c, contexts, vid, vwcet);
  else
  {
    generateConstraints_IPET_selected_method(os, c, vn, ve, contexts, vid, vwcet);
    // Generate a constraint for every BB / edge for every execution context
    // ---------------------------------------------------------------------
    generateConstraints_BB_edge_eachContext(c, os, vn, ve, contexts);
    generateConstraints_back_edges_loops(c, os, vn, contexts);
  }
  return true;
}

bool CustomIPETAnalysis::isDeadCode(Cfg *cfg)
{
  // Disable dead code elimination for now
  return false;
  if (call_graph->isDeadCode(cfg))
    return true;

  const ContextList &contexts = (ContextList &)cfg->GetAttribute(ContextListAttributeName);
  for (Context *callee_context : contexts)
  {
    Node *n = callee_context->getCallerNode();
    if (!n || (!n->GetCfg()->HasAttribute(ExternalWCETAttributeName) && !isDeadCode(n->GetCfg())))
      return false;
  }
  return true;
}

/// ------------------------------------------------
/// Allow a basic bloc to be executed even if it's function has
/// not been executed (to allow checkpoint placed in functions)
///
/// A basic block right after a call:
/// - can be executed if it's predecessor has been executed
/// - can be executed if we went out of a function without executing
///   it's entry point
///
/// How do we know that we returned from a function without executing
/// it's entry point?
///
/// We check if the sum of it's return basic bloc executed matches
/// the number of time the entry point has been executed
///
/// SUM n_entry_c0 + n_entry_c1 + ... - n_ret_c0 - ... = 0 => We always
/// entered the function from it's entry point
///
/// @param os the stream where constraints should be generated
/// @param cfg the Cfg for which constraints have to be generated
void CustomIPETAnalysis::generateEdgesForFunctionReturn(ostringstream &os, Cfg *cfg)
{
  vector<Node *> retBBs = AnalysisHelper::getReturnNodes(cfg);
  vector<Node *> chkptBBs = AnalysisHelper::getCheckpointNodes(cfg);

  // Do not generate return edges for function checkpoint
  if (AnalysisHelper::isCheckpointFn(cfg))
  {
    return;
  }
  // First, generate a sum of all the return nodes + the entry point
  // At this point, the sum is zero if the function has been called normally
  const ContextList &contexts = (ContextList &)cfg->GetAttribute(ContextListAttributeName);
  for (size_t ci = 0; ci < contexts.size(); ci++)
  {
    ostringstream cs;
    Context *context = contexts[ci];

    if (context->getCallerContext() == nullptr)
      continue;

    Node *call = context->getCallerNode();

    string caller_context = context->getCallerContext()->getStringId();
    // Add the return nodes
    for (size_t ri = 0; ri < retBBs.size(); ri++)
    {
      cs << mkVariableNameCustomSolver("n_", retBBs[ri], context->getStringId());
      if (ri != retBBs.size() - 1)
        cs << " + ";
    }

    // For all outgoing edges
    for (Edge *e : call->GetCfg()->GetOutgoingEdges(call))
    {
      string return_point_id = mkVariableNameCustomSolver("n_", e->GetTarget(), caller_context);
      AnalysisHelper::SetBoolAttr(e, EdgeDeactivated);
      cs << " - e_" << cfg->GetName().back() << "_c" << context->getStringId() << "_";
      cs << return_point_id;
    }
    cs << " = 0";
    cs << endl;
    os << cs.str();
  }
}

/// ------------------------------------------------
/// Generate additional constraints to disable edges
/// that are not part of the WCET computation
///
/// Does not work at all. At the moment, it set to 0 the edges outside
/// of the subgraph which is not what we want.
/// We want a way to "unlink" the edges from the subgraph. For now, it just
/// impose the basic block preceding the subgraph not to be executed.
///
/// @param os the stream where constraints should be generated
/// @param cfg the Cfg for which constraints have to be generated
/// ------------------------------------------------
void CustomIPETAnalysis::generateEntryPointConstraints(ostringstream &os, Cfg *cfg)
{
  // Get the entry point
  Node *entry_point;
  bool entry_point_found = false;
  for (Node *n : cfg->GetAllNodes())
  {
    if (n->HasAttribute(AnalysisStartingPointAttribute))
    {
      entry_point = n;
      entry_point_found = true;
      break;
    }
  }
  if (!entry_point_found)
  {
    entry_point = p->GetEntryPoint()->GetStartNode();
  }
  // assert(entry_point_found == true && "ENTRY POINT NOT FOUND");
  const ContextList &contexts = (ContextList &)cfg->GetAttribute(ContextListAttributeName);
  // Get the incoming edges of the entry point
  vector<Edge *> incoming_edges = cfg->GetIncomingEdges(entry_point);
  vector<string> vs;

  // If the entry point correspond to the entry of the function (a.k.a a real entry point)
  // Set this node number of execution to 1 (as we will execute it one time)
  // Else, create a fake node `n_entry` that will be the entry point of the function, and create
  // a node from `n_entry` to the entry point of the function, e.g for bb0 :
  // ```
  // n_entry = 1
  // e_entry_n_0 - n_entry = 0
  // e_entry_n_0 + ... - n_0 = 0
  // ```
  if (incoming_edges.size() == 0)
  {
    long start_id = getIntegerAttribute(entry_point, InternalAttributeId);
    ostringstream nid;
    nid << start_id;
    for (Context *context : contexts)
    {
      vs.push_back("n_" + nid.str() + "_c" + context->getStringId());
    }
    solver->generate_equality(os, vs, 1);
  }
  else
  {
    vs.push_back("n_entry");
    for (Context *c : contexts)
    {
      vs.push_back(mkVariableNameCustomSolver("n_entry_", entry_point, c->getStringId()));
    }
    if (vs.size() > 0)
    {
      solver->generate_flow_constraint(os, vs);
      vs.clear();
      vs.push_back("n_entry");
      solver->generate_equality(os, vs, 1);
    }
    else
    {
      new exception();
    }
  }
}

// -------------------------------------------
// Deactivate the outgoing edges of call basic blocks
//
// The IPET generation is modified to "inline" functions, and
// new links are created between the callee exit point and the caller
// successors.
//
// It is thus necessary to remove the egdes between a basic block
// containing a function call and it's successors
// -------------------------------------------
void CustomIPETAnalysis::deactivateCallOutgoingEdges(ostringstream &os, Cfg *cfg)
{
  vector<Node *> callNodes = cfg->GetCallNodes();
  for (Node *node : callNodes)
  {
    vector<Edge *> outgoing_edges = cfg->GetOutgoingEdges(node);
    for (Edge *edge : outgoing_edges)
    {
      AnalysisHelper::SetBoolAttr(edge, EdgeDeactivated);
    }
  }
}

// -------------------------------------------
// Core of the analysis
// generate an ILP problem to compute
// the program WCET
// -------------------------------------------
bool CustomIPETAnalysis::PerformAnalysis()
{
  if (method == NOT_YET_IMPLEMENTED)
    return false;

  ostringstream strc; // objective function
  ostringstream strf; // flow constraints
  ostringstream stde; // declarations

  Cfg *function = p->GetEntryPoint();
  assert(!isNULLPointer(function));
  assert(!function->HasAttribute(WCETAttributeName));
  if (function->IsEmpty() || function->HasAttribute(ExternalWCETAttributeName))
  {
    WCET_type wcet = 0;
    if (function->HasAttribute(ExternalWCETAttributeName))
    {
      ExternalWCETAttributeNameType &ia = (ExternalWCETAttributeNameType &)function->GetAttribute(ExternalWCETAttributeName);
      wcet = ia.GetValue();
    }
    else
      Logger::addWarning("Function " + function->getStringName() + " is empty - WCET=" + to_string(wcet));
    ExternalWCETAttributeNameType ba(wcet);
    function->SetAttribute(WCETAttributeName, ba);
    return true;
  }

  char buffer[31] = "/tmp/CustomIPETAnalysis_XXXXXX";
  mkstemp(buffer);
  ofstream os(buffer);
  string fout = buffer;
  cout << "CustomSolver input file: " << buffer << endl;

  // Get the Cfg of the program entry point
  // --------------------------------------
  vector<string> vid;
  VECTOR_WCET vwcet;
  vector<Cfg *> lcfg = p->GetAllCfgs();
  for (unsigned int c = 0; c < lcfg.size(); c++)
  {
    generateNodeIds(strc, lcfg[c]);
  }

  // Constraint for entry point
  strc << "\\ *** Constraints for entry point ****" << endl;
  generateEntryPointConstraints(strc, function);
  strc << endl;
  for (unsigned int c = 0; c < lcfg.size(); c++)
  {
    strc << "\\ *** Constraints for " << lcfg[c]->GetName().back() << "****" << endl;
    deactivateCallOutgoingEdges(strc, lcfg[c]);
    if (lcfg[c]->IsEmpty())
      continue;
    // if (!isDeadCode(lcfg[c]))
    generateConstraints(strc, lcfg[c], vid, vwcet);
    if (!AnalysisHelper::isCheckpointFn(lcfg[c]))
      generateEdgesForFunctionReturn(strc, lcfg[c]);
  }

  generateCallConstraints(strc, p);

  solver->generate_objective_function(strf, vid, vwcet);
  solver->generate_declarations(stde, AnalysisHelper::unicity(vid));

  // Write everything (objective first, constraints, then declarations last) in the output file Objective function
  os << strf.str();
  // All the constraints (except statistics)
  os << strc.str();
  // Declarations
  os << stde.str();
  os.close();

  // Launch the solver
  string tmpFileName;
  if (!Utl::mktmpfile("/tmp/solver_", tmpFileName))
    return false;

  cout << "CustomSolver output file: " << tmpFileName << endl;
  if (!solver->solve(fout, tmpFileName))
    return false;

  // Parse the solver output
  string wcet;
  solver->parse_output(tmpFileName, wcet);

  // Attach result to entry point
  if (this->generate_wcet_information)
  {
    SerialisableStringAttribute ba(wcet);
    function->SetAttribute(WCETAttributeName, ba);
  }
  return true;
}
