#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "TestUtils.h"

TEST_CASE("DEACTIVATE_EDGE_FUNCTION")
{
    setup_heptane_analysis("TestIPETFunction");
    Cfg *main = config->getEntryPoint();
    Program *p = main->GetProgram();
    Cfg *foo = p->GetCfgByName("foo");
    assert(foo != 0);
    // Get foo basic blocks
    Node *bb1 = foo->GetStartNode();
    REQUIRE(foo->GetSuccessors(bb1).size() == 2);
    Node *bb2 = foo->GetSuccessors(bb1)[0];
    Node *bb4 = foo->GetSuccessors(bb1)[1];
    REQUIRE(foo->GetSuccessors(bb2).size() == 1);
    Node *bb3 = foo->GetSuccessors(bb2)[0];
    REQUIRE(foo->GetSuccessors(bb4).size() == 1);
    Node *bb5 = foo->GetSuccessors(bb4)[0];
    REQUIRE(foo->GetSuccessors(bb4)[0]==bb5);
    // Get Main basic blocks
    Node *bb6 = main->GetStartNode();
    REQUIRE(main->GetSuccessors(bb6).size() == 1);
    Node *bb7 = main->GetSuccessors(bb6)[0];

    CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

    SECTION("CHECKPOINT_IN_FUNCTION")
    {
        // Checkpoint function call in BB2
        AnalysisHelper::SetBoolAttr(bb2, CheckpointBB);
        // Launch the analysis from BB6
        ci->DeactivateUnreachableEdges(p, bb6);

        // Make sure (BB2, BB3), (BB3, BB5) and (BB6,BB7) are deactivated
        REQUIRE(edge_is_deactivated(foo, bb2, bb3));
        REQUIRE(edge_is_deactivated(foo, bb3, bb5));
        REQUIRE(edge_is_deactivated(main, bb6, bb7));

        REQUIRE(!edge_is_deactivated(foo, bb1, bb2));
        REQUIRE(!edge_is_deactivated(foo, bb1, bb4));
        REQUIRE(!edge_is_deactivated(foo, bb4, bb5));

        // REQUIRE(edge_is_deactivated(main, bb6, bb7));

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);
        bb2->RemoveAttribute(CheckpointBB);
    }

    SECTION("CHECKPOINT_IN_FUNCTION_PROLOG")
    {
        bb2->RemoveAttribute(CheckpointBB);
        // Checkpoint function call in BB1
        AnalysisHelper::SetBoolAttr(bb1, CheckpointBB);
        // Launch the analysis from BB6
        ci->DeactivateUnreachableEdges(p, bb6);
        // Make sure the edges (BB1, BB2), (BB2, BB3), (BB3, BB5),
        // (BB1, BB4), (BB4, BB5)  are deactivated
        REQUIRE(edge_is_deactivated(foo, bb1, bb2));
        REQUIRE(edge_is_deactivated(foo, bb2, bb3));
        REQUIRE(edge_is_deactivated(foo, bb3, bb5));
        REQUIRE(edge_is_deactivated(foo, bb1, bb4));
        REQUIRE(edge_is_deactivated(foo, bb4, bb5));
        REQUIRE(edge_is_deactivated(main, bb6, bb7));

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);


        // Launch the analysis from BB2
        ci->DeactivateUnreachableEdges(p, bb2);
        // Deactivate the edges (BB1, BB2), (BB1, BB4), (BB4, BB5) and (BB6,BB7)
        REQUIRE(edge_is_deactivated(foo, bb1, bb2));
        REQUIRE(edge_is_deactivated(foo, bb1, bb4));
        REQUIRE(edge_is_deactivated(foo, bb4, bb5));

        REQUIRE(edge_is_deactivated(main, bb6, bb7));
        REQUIRE(edge_is_deactivated(foo, bb2, bb3)); // Deactivated as it is a function call
        REQUIRE(!edge_is_deactivated(foo, bb3, bb5));

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Launch the analysis from BB4
        ci->DeactivateUnreachableEdges(p, bb4);
        // Deactivate the edges (BB1, BB2), (BB1, BB4), (BB2, BB3) (BB4, BB5) and (BB6,BB7)
        REQUIRE(edge_is_deactivated(foo, bb1, bb2));
        REQUIRE(edge_is_deactivated(foo, bb1, bb4));
        REQUIRE(edge_is_deactivated(foo, bb2, bb3));
        REQUIRE(edge_is_deactivated(foo, bb3, bb5));

        REQUIRE(edge_is_deactivated(main, bb6, bb7));
        REQUIRE(!edge_is_deactivated(foo, bb4, bb5));

        bb1->RemoveAttribute(CheckpointBB);
    }
}
