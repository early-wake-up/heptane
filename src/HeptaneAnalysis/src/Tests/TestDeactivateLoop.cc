#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "TestUtils.h"

TEST_CASE("DEACTIVATE_EDGE_BEFORE_LOOP") {
   setup_heptane_analysis("TestIPETLoopCheckpointBB1");
   Cfg *main = config->getEntryPoint();
   Program *p = main->GetProgram();
   Cfg *chkpt = p->GetCfgByName("checkpoint");
   assert(chkpt != NULL && "Could not found checkpoint cfg in loop.xml");
   SerialisableIntegerAttribute attr(1);
   chkpt->SetAttribute(CheckpointFn, attr);

   CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

   Node *bb1 = main->GetStartNode();
   REQUIRE(main->GetSuccessors(bb1).size() == 1);
   Node *bb2 = main->GetSuccessors(bb1)[0];
   REQUIRE(main->GetSuccessors(bb2).size() == 1);
   Node *bb3 = main->GetSuccessors(bb2)[0];
   REQUIRE(main->GetSuccessors(bb3).size() == 2);
   Node *bb4 = main->GetSuccessors(bb3)[0];
   Node *bb5 = main->GetSuccessors(bb3)[1];

    SECTION("CHECKPOINT_BEFORE_LOOP")
    {
        // Checkpoint is in bb1
        AnalysisHelper::SetBoolAttr(bb1, CheckpointBB);

        ci->DeactivateUnreachableEdges(p, bb1);

        REQUIRE(edge_is_deactivated(main, bb1, bb2));
        REQUIRE(edge_is_deactivated(main, bb2, bb3));
        REQUIRE(edge_is_deactivated(main, bb3, bb5));
        REQUIRE(edge_is_deactivated(main, bb3, bb4));
        REQUIRE(edge_is_deactivated(main, bb4, bb3));

        ci->RemovePrivateAttributes();
        // Assert that all private attributes have been removed
        IPET_assert_cleaned(p);

        ci->DeactivateUnreachableEdges(p, bb2);

        REQUIRE(edge_is_deactivated(main, bb1, bb2));
        REQUIRE(!edge_is_deactivated(main, bb2, bb3));
        REQUIRE(!edge_is_deactivated(main, bb3, bb5));
        REQUIRE(!edge_is_deactivated(main, bb3, bb4));
        REQUIRE(!edge_is_deactivated(main, bb4, bb3));


        ci->RemovePrivateAttributes();
        // Assert that all private attributes have been removed
        IPET_assert_cleaned(p);
        bb1->RemoveAttribute(CheckpointBB);
    }
}

TEST_CASE("DEACTIVATE_EDGE_LOOP", "Check if the edges are well deactivated for a loop, with different checkpoint placements")
{
    setup_heptane_analysis("TestIPETLoop");
    Cfg *main = config->getEntryPoint();
    Program *p = main->GetProgram();
    Node *bb1 = main->GetStartNode();
    REQUIRE(main->GetSuccessors(bb1).size() == 1);
    Node *bb2 = main->GetSuccessors(bb1)[0];
    REQUIRE(main->GetSuccessors(bb2).size() == 2);
    Node *bb3 = main->GetSuccessors(bb2)[0];
    Node *bb5 = main->GetSuccessors(bb2)[1];
    REQUIRE(AnalysisHelper::getStartAddress(bb3) == 0x441e);
    REQUIRE(AnalysisHelper::getStartAddress(bb5) == 0x443a);
    REQUIRE(main->GetSuccessors(bb3).size() == 1);
    Node *bb4 = main->GetSuccessors(bb3)[0];
    CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

    SECTION("GENERATE_IPET_CHECKPOINT_LOOP_BODY", "??")
    {
        // Checkpoint in BB3
        AnalysisHelper::SetBoolAttr(bb3, CheckpointBB);

        // Analysis from BB1 (Entry point of main)
        ci->DeactivateUnreachableEdges(p, bb1);

        // Edges (BB3, BB4) and (BB4, BB2) must be deactivated
        REQUIRE(edge_is_deactivated(main, bb3, bb4));
        REQUIRE(edge_is_deactivated(main, bb4, bb2));
        // The other edges must be activated
        REQUIRE(!edge_is_deactivated(main, bb2, bb3));
        REQUIRE(!edge_is_deactivated(main, bb2, bb5));
        REQUIRE(!edge_is_deactivated(main, bb1, bb2));
        deactivate_edge(main, bb3, bb4);
        deactivate_edge(main, bb4, bb2);

        // Remove all attributes
        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Analysis from BB4
        ci->DeactivateUnreachableEdges(p, bb4);
        // Edges (BB1, BB2) and (BB3, BB4) must be deactivated
        REQUIRE(edge_is_deactivated(main, bb1, bb2));
        REQUIRE(edge_is_deactivated(main, bb3, bb4));
        // The other edges must be activated
        REQUIRE(!edge_is_deactivated(main, bb2, bb3));
        REQUIRE(!edge_is_deactivated(main, bb2, bb5));
        REQUIRE(!edge_is_deactivated(main, bb4, bb2));


        bb1->RemoveAttribute(CheckpointBB);
    }

    /// Test the IPET analysis with a checkpoint on the loop header
    SECTION("GENERATE_IPET_CHECKPOINT_LOOP_HEADER", "??")
    {
        // Checkpoint in BB2
        AnalysisHelper::SetBoolAttr(bb2, CheckpointBB);

        // Analysis from BB1 (Entry point of main)
        ci->DeactivateUnreachableEdges(p, bb1);
        // Edges (BB2, BB3), (BB3, BB4), (BB4, BB2) and (BB2, BB5) must be deactivated
        REQUIRE(edge_is_deactivated(main, bb2, bb3));
        REQUIRE(edge_is_deactivated(main, bb3, bb4));
        REQUIRE(edge_is_deactivated(main, bb4, bb2));
        REQUIRE(edge_is_deactivated(main, bb2, bb5));
        REQUIRE(!edge_is_deactivated(main, bb1, bb2));

        // Remove all attributes
        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Analysis from BB3
        ci->DeactivateUnreachableEdges(p, bb3);
        // Edges (BB1, BB2), (BB2, BB3) and (BB2, BB5) must be deactivated
        REQUIRE(edge_is_deactivated(main, bb1, bb2));
        REQUIRE(edge_is_deactivated(main, bb2, bb3));
        REQUIRE(edge_is_deactivated(main, bb2, bb5));
        REQUIRE(!edge_is_deactivated(main, bb4, bb2));

        // Remove all attributes
        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        bb2->RemoveAttribute(CheckpointBB);
    }
}
