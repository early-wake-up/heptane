#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"

/// Simple test case to check that the edges are deactivated correctly
/// in a simple cfg with one checkpoint and no alternative
///
/// BB0: SUB.W	#6, R1
///      call checkpoint
///
/// BB1: ADD.W	#6, R1
///      ret
///
///-----------------
TEST_CASE( "Test deactivate edges simple cfg 2bb", "??") {
    Program *p = new Program();
    Cfg *cfg = p->CreateNewCfg(ListOfString({"","main"}));
   //new Cfg(p, ListOfString({"","foo"}));
    // new Cfg(p, ListOfString({"","checkpoint"}));
    // Create BB0
    Node * bb0 = cfg->CreateNewNode(BB);
    bb0->CreateNewInstruction("sub	#6,	r1	",cfglib::Code, false);
    bb0->CreateNewInstruction("call	#17410", cfglib::Code, false);
    AnalysisHelper::SetBoolAttr(bb0, CheckpointBB);
    // Create BB1
    Node * bb1 = cfg->CreateNewNode(BB);
    bb1->CreateNewInstruction("add	#6,	r1	",cfglib::Code, false);
    bb1->CreateNewInstruction("ret", cfglib::Code, true);

    Edge * e = cfg->CreateNewEdge(bb0, bb1);

    CheckpointIsolation * ci = new CheckpointIsolation(p, "blabla");
    int nb_deactivated = ci->DeactivateUnreachableEdges(p, bb0);
    REQUIRE( nb_deactivated == 1 );
    REQUIRE( e->HasAttribute(EdgeDeactivated) == true );
    e->RemoveAttribute(EdgeDeactivated);
    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb1);
    REQUIRE( nb_deactivated == 1 );
    REQUIRE( e->HasAttribute(EdgeDeactivated) == true );
    bb0->RemoveAttribute(CheckpointBB);
}

/// Simple test case to check that the edges are deactivated correctly
/// in a simple cfg with one checkpoint and no alternative
///
/// BB0: SUB.W	#6, R1
///      call checkpoint
///
/// BB1: ADD.W	#6, R1
///      call foo
///
/// BB2: ADD.W	#6, R1
///      ret
///
///-----------------
TEST_CASE( "Test deactivate edges simple cfg 3bb", "??") {
    Program *p = new Program();
    Cfg *cfg = p->CreateNewCfg(ListOfString({"","main"}));
   //new Cfg(p, ListOfString({"","foo"}));
    // new Cfg(p, ListOfString({"","checkpoint"}));
    // Create BB0
    Node * bb0 = cfg->CreateNewNode(BB);
    bb0->CreateNewInstruction("sub	#6,	r1	",cfglib::Code, false);
    bb0->CreateNewInstruction("call	#17410", cfglib::Code, false);
    AnalysisHelper::SetBoolAttr(bb0, CheckpointBB);
    // Create BB1
    Node * bb1 = cfg->CreateNewNode(BB);
    bb1->CreateNewInstruction("add	#6,	r1	",cfglib::Code, false);
    // Create BB1
    Node * bb2 = cfg->CreateNewNode(BB);
    bb2->CreateNewInstruction("add	#6,	r1	",cfglib::Code, false);
    bb2->CreateNewInstruction("ret", cfglib::Code, true);

    Edge * e_0_1 = cfg->CreateNewEdge(bb0, bb1);
    Edge * e_1_2 = cfg->CreateNewEdge(bb1, bb2);

    CheckpointIsolation * ci = new CheckpointIsolation(p, "blabla");
    int nb_deactivated = ci->DeactivateUnreachableEdges(p, bb0);
    REQUIRE( nb_deactivated == 2 );
    REQUIRE( e_0_1->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_1_2->HasAttribute(EdgeDeactivated) == true );
    e_0_1->RemoveAttribute(EdgeDeactivated);
    e_1_2->RemoveAttribute(EdgeDeactivated);

    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb1);
    REQUIRE( nb_deactivated == 1 );
    REQUIRE( e_0_1->HasAttribute(EdgeDeactivated) == true );
    e_0_1->RemoveAttribute(EdgeDeactivated);

    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb2);
    REQUIRE( nb_deactivated == 2 );
    REQUIRE( e_0_1->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_1_2->HasAttribute(EdgeDeactivated) == true );

    bb0->RemoveAttribute(CheckpointBB);
}

/// Loop test case to check that the edges are deactivated correctly
///
/// BB0: SUB.W	#6, R1
///      call checkpoint
///
/// BB1: ADD.W	#6, R1
///      br BB0
/// -----------------
TEST_CASE( "Test deactivate edges loop cfg 2bb", "??") {
    Program *p = new Program();
    Cfg *cfg = p->CreateNewCfg(ListOfString({"","main"}));
   //new Cfg(p, ListOfString({"","foo"}));
    // new Cfg(p, ListOfString({"","checkpoint"}));
    // Create BB0
    Node * bb0 = cfg->CreateNewNode(BB);
    bb0->CreateNewInstruction("sub	#6,	r1	",cfglib::Code, false);
    bb0->CreateNewInstruction("call	#17410", cfglib::Code, false);
    AnalysisHelper::SetBoolAttr(bb0, CheckpointBB);
    // Create BB1
    Node * bb1 = cfg->CreateNewNode(BB);
    bb1->CreateNewInstruction("add	#6,	r1	",cfglib::Code, false);
    bb1->CreateNewInstruction("br bb0", cfglib::Code, false);

    Edge * e = cfg->CreateNewEdge(bb0, bb1);
    Edge * e_loop = cfg->CreateNewEdge(bb1, bb0);

    CheckpointIsolation * ci = new CheckpointIsolation(p, "blabla");
    int nb_deactivated = ci->DeactivateUnreachableEdges(p, bb0);
    REQUIRE( nb_deactivated == 2 );
    REQUIRE( e->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_loop->HasAttribute(EdgeDeactivated) == true );
    e->RemoveAttribute(EdgeDeactivated);
    e_loop->RemoveAttribute(EdgeDeactivated);

    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb1);
    REQUIRE( nb_deactivated == 1 );
    REQUIRE( e->HasAttribute(EdgeDeactivated) == true );

    bb0->RemoveAttribute(CheckpointBB);
}

/// Loop test case to check that the edges are deactivated correctly
///
/// BB0: SUB.W	#6, R1
///      cmp #0, r1
///      br bb3
///
/// BB1: ADD.W	#6, R1
///      call checkpoint
///
/// BB2: br bb0
///
/// BB3: ret
///                                 -------
///   /---------------------------> | BB3 |
/// ------    ------     -------     -------
/// |BB0 | -> | BB1 | -> | BB2 |
/// ------    ------     -------
///    ^-------------------/
///
/// -----------------
TEST_CASE( "Test deactivate edges loop cfg 4bb", "??") {
    Program *p = new Program();
    Cfg *cfg = p->CreateNewCfg(ListOfString({"","main"}));
    // Create BB0
    Node * bb0 = cfg->CreateNewNode(BB);
    bb0->CreateNewInstruction("sub	#6,	r1	",cfglib::Code, false);
    bb0->CreateNewInstruction("cmp	#0,	r1	",cfglib::Code, false);
    bb0->CreateNewInstruction("br bb3", cfglib::Code, false);

    // Create BB1
    Node * bb1 = cfg->CreateNewNode(BB);
    bb1->CreateNewInstruction("add	#6,	r1	",cfglib::Code, false);
    bb1->CreateNewInstruction("call	#17410", cfglib::Code, false);
    AnalysisHelper::SetBoolAttr(bb1, CheckpointBB);

    // Create BB2
    Node * bb2 = cfg->CreateNewNode(BB);
    bb2->CreateNewInstruction("br bb0", cfglib::Code, false);

    // Create BB3
    Node * bb3 = cfg->CreateNewNode(BB);
    bb3->CreateNewInstruction("ret", cfglib::Code, true);

    Edge * e_0_1 = cfg->CreateNewEdge(bb0, bb1);
    Edge * e_1_2 = cfg->CreateNewEdge(bb1, bb2);
    Edge * e_2_0 = cfg->CreateNewEdge(bb2, bb0);
    Edge * e_0_3 = cfg->CreateNewEdge(bb0, bb3);

    CheckpointIsolation * ci = new CheckpointIsolation(p, "blabla");
    int nb_deactivated = ci->DeactivateUnreachableEdges(p, bb0);
    REQUIRE( nb_deactivated == 2 );
    REQUIRE( e_1_2->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_2_0->HasAttribute(EdgeDeactivated) == true );
    e_1_2->RemoveAttribute(EdgeDeactivated);
    e_2_0->RemoveAttribute(EdgeDeactivated);

    // BB1 has a checkpoint, so all is deactivated
    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb1);
    REQUIRE( nb_deactivated == 4 );
    REQUIRE( e_0_1->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_1_2->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_2_0->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_0_3->HasAttribute(EdgeDeactivated) == true );
    e_0_1->RemoveAttribute(EdgeDeactivated);
    e_1_2->RemoveAttribute(EdgeDeactivated);
    e_2_0->RemoveAttribute(EdgeDeactivated);
    e_0_3->RemoveAttribute(EdgeDeactivated);

    // BB2 is the loop latch. it can go to all other BBs
    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb2);
    REQUIRE( nb_deactivated == 1 );
    REQUIRE( e_1_2->HasAttribute(EdgeDeactivated) == true );
    e_1_2->RemoveAttribute(EdgeDeactivated);

    // BB3 is the exit node. it cannot go to any other BB
    nb_deactivated = ci->DeactivateUnreachableEdges(p, bb3);
    REQUIRE( nb_deactivated == 4 );
    REQUIRE( e_0_1->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_1_2->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_2_0->HasAttribute(EdgeDeactivated) == true );
    REQUIRE( e_0_3->HasAttribute(EdgeDeactivated) == true );
    e_0_1->RemoveAttribute(EdgeDeactivated);
    e_1_2->RemoveAttribute(EdgeDeactivated);
    e_2_0->RemoveAttribute(EdgeDeactivated);
    e_0_3->RemoveAttribute(EdgeDeactivated);

    bb1->RemoveAttribute(CheckpointBB);
}
