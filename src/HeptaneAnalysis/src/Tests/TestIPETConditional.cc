#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "TestUtils.h"

TEST_CASE("GENERATE_IPET_CONDITIONAL_TEST")
{
    setup_heptane_analysis("TestIPETConditional");
    Cfg *main = config->getEntryPoint();
    Program *p = main->GetProgram();
    Node *bb1 = main->GetStartNode();
    REQUIRE(main->GetSuccessors(bb1).size() == 2);
    Node *bb2 = main->GetSuccessors(bb1)[0];
    Node *bb4 = main->GetSuccessors(bb1)[1];
    REQUIRE(main->GetSuccessors(bb2).size() == 1);
    Node *bb3 = main->GetSuccessors(bb2)[0];
    REQUIRE(main->GetSuccessors(bb4).size() == 1);
    Node *bb5 = main->GetSuccessors(bb4)[0];

    CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

    SECTION("CHECKPOINT_CONDITIONAL_START")
    {
        long wcet;
        // Deactivate the edges (BB1,BB2) and (BB1,BB4)
        deactivate_edge(main, bb1, bb2);
        deactivate_edge(main, bb1, bb4);
        // Launch the IPET analysis from BB1
        // Should return 8 as only the basic block BB1 is executed
        wcet = ci->launchIpetAnalysis(p, bb1);
        REQUIRE(wcet == 8);

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Deactivate the edges (BB1,BB2) and (BB1,BB4)
        deactivate_edge(main, bb1, bb2);
        deactivate_edge(main, bb1, bb4);
        deactivate_edge(main, bb4, bb5);
        // Launch the IPET analysis from BB2
        // Path: BB2 -> BB3 -> BB5
        // WCET:  4  +   1  +  5  = 8
        wcet = ci->launchIpetAnalysis(p, bb2);
        REQUIRE(wcet == 10);

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Deactivate the edges (BB1,BB2) and (BB1,BB4)
        deactivate_edge(main, bb1, bb2);
        deactivate_edge(main, bb2, bb3);
        deactivate_edge(main, bb3, bb5);
        deactivate_edge(main, bb1, bb4);
        // Launch the IPET analysis from BB4
        // Path: BB4 -> BB5
        // WCET:  1  +  5  = 6
        wcet = ci->launchIpetAnalysis(p, bb4);
        REQUIRE(wcet == 6);
    }

    SECTION("CHECKPOINT_IN_CONDITION")
    {
        // Checkpoint function call in BB2

        // Analyze from BB1
        // Deactivate the edges (BB2,BB3) and (BB3,BB5)
        deactivate_edge(main, bb2, bb3);
        deactivate_edge(main, bb3, bb5);
        // Launch the analysis from BB1
        // Path 1: BB1 -> BB2
        //  WCET :  8   +  4 = 12
        // Path 2: BB1 -> BB4 -> BB5
        //  WCET :  8   +  1  +  5 = 14
        long wcet = ci->launchIpetAnalysis(p, bb1);
        REQUIRE(wcet == 14);

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Analyze from BB3
        // Deactivate the edges (BB1, BB2), (BB1,BB4), (BB2,BB3) and (BB4,BB5)
        deactivate_edge(main, bb1, bb2);
        deactivate_edge(main, bb1, bb4);
        deactivate_edge(main, bb2, bb3);
        deactivate_edge(main, bb4, bb5);

        // Launch the analysis from BB3
        // Path: BB3 -> BB5
        //  WCET:  1  +  5 = 6
        wcet = ci->launchIpetAnalysis(p, bb3);
        REQUIRE(wcet == 6);
    }
}
