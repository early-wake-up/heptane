#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "TestUtils.h"

TEST_CASE("GENERATE_IPET_FUNCTION_TEST", "[selected]")
{
    setup_heptane_analysis("TestIPETFunction");
    Cfg *main = config->getEntryPoint();
    Program *p = main->GetProgram();
    Cfg *foo = p->GetCfgByName("foo");
    assert(foo != 0);
    // Get foo basic blocks
    Node *bb1 = foo->GetStartNode();
    REQUIRE(foo->GetSuccessors(bb1).size() == 2);
    Node *bb2 = foo->GetSuccessors(bb1)[0];
    Node *bb4 = foo->GetSuccessors(bb1)[1];
    REQUIRE(foo->GetSuccessors(bb2).size() == 1);
    Node *bb3 = foo->GetSuccessors(bb2)[0];
    REQUIRE(foo->GetSuccessors(bb4).size() == 1);
    Node *bb5 = foo->GetSuccessors(bb4)[0];

    // Get Main basic blocks
    Node *bb6 = main->GetStartNode();
    REQUIRE(main->GetSuccessors(bb6).size() == 1);
    Node *bb7 = main->GetSuccessors(bb6)[0];

    CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

    SECTION("CHECKPOINT_IN_FUNCTION")
    {
        // Checkpoint function call in BB2

        // Launch the IPET analysis from BB6
        // Deactivate the edges (BB2, BB3), (BB3, BB5) and (BB6,BB7)
        deactivate_edge(foo, bb2, bb3);
        deactivate_edge(foo, bb3, bb5);
        deactivate_edge(main, bb6, bb7);

        // Launch the analysis from BB6
        // Path 1: BB6 (call foo) -> BB1 -> BB4 -> BB5 + bb7
        //  WCET :  8  +              5   +  1  +  3 +   3 = 20
        // Path 2: BB6 (call foo) -> BB1 -> BB2
        //  WCET :  8  +              5   +  3  = 16
        long wcet = ci->launchIpetAnalysis(p, bb6);
        REQUIRE(wcet == 20);

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);
    }

    SECTION("CHECKPOINT_IN_FUNCTION_PROLOG", "[selected]")
    {
        // Checkpoint function call in BB1

        // Launch the IPET analysis from BB6
        // Deactivate the edges (BB1, BB2), (BB2, BB3), (BB3, BB5),
        // (BB1, BB4), (BB4, BB5) and (BB6,BB7)
        deactivate_edge(foo, bb1, bb2);
        deactivate_edge(foo, bb2, bb3);
        deactivate_edge(foo, bb3, bb5);
        deactivate_edge(foo, bb1, bb4);
        deactivate_edge(foo, bb4, bb5);
        // deactivate_edge(main, bb6, bb7);

        AnalysisHelper::SetBoolAttr(bb1, CheckpointBB);
        // Launch the analysis from BB6
        // Path  : BB6 (call foo) -> BB1
        //  WCET :  8  +              5   = 13
        long wcet = ci->launchIpetAnalysis(p, bb6);
        REQUIRE(wcet == 13);

        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);
        // Launch the IPET analysis from BB2
        // Deactivate the edges (BB1, BB2), (BB1, BB4), (BB4, BB5) and (BB6,BB7)
        deactivate_edge(foo, bb1, bb2);
        deactivate_edge(foo, bb1, bb4);
        deactivate_edge(foo, bb4, bb5);
        // deactivate_edge(main, bb6, bb7);
        // Launch the analysis from BB2
        // Path 1: BB2 -> BB3 -> BB5 -> BB7
        //  WCET :  1 (+call 2)  +   2  +   3  +  3  =  9
        wcet = ci->launchIpetAnalysis(p, bb2);
        REQUIRE(wcet == 11);


        ci->RemovePrivateAttributes();
        IPET_assert_cleaned(p);

        // Launch the IPET analysis from BB4
        // Deactivate the edges (BB1, BB2), (BB1, BB4), (BB3, BB5)
        deactivate_edge(foo, bb1, bb2);
        deactivate_edge(foo, bb1, bb4);
        deactivate_edge(foo, bb2, bb3);
        deactivate_edge(foo, bb3, bb5);
        // deactivate_edge(main, bb6, bb7);
        // Launch the analysis from BB4
        // Path 1: BB4 -> BB5 -> BB7
        //  WCET :  1  +   3  +  3  =  7
        wcet = ci->launchIpetAnalysis(p, bb4);
        REQUIRE(wcet == 7);

        bb1->RemoveAttribute(CheckpointBB);
    }
}
