#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "TestUtils.h"

TEST_CASE("Generate an IPET for a loop", "??")
{
   setup_heptane_analysis("TestIPETLoop");
   Cfg *main = config->getEntryPoint();
   Program *p = main->GetProgram();
   CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

   long wcet = ci->launchIpetAnalysis(p, main->GetStartNode());
   REQUIRE(wcet == 104);
}

TEST_CASE("GENERATE_IPET_LOOP_CHKPT_BEFORE_LOOP")
{
   setup_heptane_analysis("TestIPETLoopCheckpointBB1");
   Cfg *main = config->getEntryPoint();
   Program *p = main->GetProgram();
   Cfg *chkpt = p->GetCfgByName("checkpoint");
   assert(chkpt != NULL && "Could not found checkpoint cfg in loop.xml");
   SerialisableIntegerAttribute attr(1);
   chkpt->SetAttribute(CheckpointFn, attr);

   CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

   Node *bb1 = main->GetStartNode();
   // Checkpoint is in bb1
   AnalysisHelper::SetBoolAttr(bb1, CheckpointBB);
   REQUIRE(main->GetSuccessors(bb1).size() == 1);
   Node *bb2 = main->GetSuccessors(bb1)[0];
   REQUIRE(main->GetSuccessors(bb2).size() == 1);
   Node *bb3 = main->GetSuccessors(bb2)[0];
   REQUIRE(main->GetSuccessors(bb3).size() == 2);
   Node *bb4 = main->GetSuccessors(bb3)[0];
   Node *bb5 = main->GetSuccessors(bb3)[1];



   // Checkpoint is in bb1, analysis from b1
   deactivate_edge(main, bb1, bb2);
   deactivate_edge(main, bb2, bb3);
   deactivate_edge(main, bb3, bb5);
   deactivate_edge(main, bb3, bb4);
   deactivate_edge(main, bb4, bb3);

   // Launch the IPET analysis from BB1
   // Should return 10 as only the basic block BB1 and BB0 (chkpt) is executed
   long wcet = ci->launchIpetAnalysis(p, bb1);
   REQUIRE(wcet == 10);

   ci->RemovePrivateAttributes();
   // Assert that all private attributes have been removed
   IPET_assert_cleaned(p);

   deactivate_edge(main, bb1, bb2);
   // Launch the IPET analysis from BB2
   // Should return 67 as the loop is executed 10 times + BB2 + BB5
   // And only BB1 is not executed
   wcet = ci->launchIpetAnalysis(p, bb2);
   REQUIRE(wcet == 67);

   ci->RemovePrivateAttributes();
   // Assert that all private attributes have been removed
   IPET_assert_cleaned(p);
}

TEST_CASE("GENERATE_IPET_LOOP")
{
   SerialisableIntegerAttribute attr(1);
   setup_heptane_analysis("TestIPETLoop");
   Cfg *main = config->getEntryPoint();
   Program *p = main->GetProgram();
   Cfg *chkpt = p->GetCfgByName("checkpoint");
   assert(chkpt != NULL && "Could not found checkpoint cfg in loop.xml");
   Node *bb1 = main->GetStartNode();
   REQUIRE(main->GetSuccessors(bb1).size() == 1);
   // Loop Header
   Node *bb2 = main->GetSuccessors(bb1)[0];
   REQUIRE(main->GetSuccessors(bb2).size() == 2);
   // Call to checkpoint ??
   Node *bb3 = main->GetSuccessors(bb2)[0];
   Node *bb5 = main->GetSuccessors(bb2)[1];
   REQUIRE(AnalysisHelper::getStartAddress(bb3) == 0x441e);
   REQUIRE(AnalysisHelper::getStartAddress(bb5) == 0x443a);
   REQUIRE(main->GetSuccessors(bb3).size() == 1);
   Node *bb4 = main->GetSuccessors(bb3)[0];
   CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");


   SECTION("GENERATE_IPET_CHECKPOINT_LOOP_BODY", "??")
   {

      bb1->RemoveAttribute(CheckpointBB);
      bb3->SetAttribute(CheckpointBB, attr);
      chkpt->SetAttribute(CheckpointFn, attr);
      // Checkpoint in BB3
      // Deactivate the edges (BB3, BB4) and (BB4, ->BB2)
      deactivate_edge(main, bb3, bb4);
      deactivate_edge(main, bb4, bb2);
      long wcet;
      // Launch the IPET analysis from BB1
      // 2 Paths: BB1 -> BB2 -> LOOP EXIT    8 + 3 + 3 = 14
      //          BB1 -> BB2 -> BB3          8 + 3 + 4 = 15
      wcet = ci->launchIpetAnalysis(p, bb1);
      REQUIRE(wcet == 15);

      // Remove all attributes
      ci->RemovePrivateAttributes();
      IPET_assert_cleaned(p);

      // Deactivate the edges (BB1, BB2) and (BB3, BB4)
      deactivate_edge(main, bb1, bb2);
      deactivate_edge(main, bb3, bb4);

      // Launch the IPET analysis from BB4
      // 2 Paths: BB4 -> BB2 -> LOOP EXIT    2 + 3 + 3 = 8
      //          BB4 -> BB2 -> BB3          2 + 3 + 4 = 9
      wcet = ci->launchIpetAnalysis(p, bb4);
      REQUIRE(wcet == 9);
   }

   /// Test the IPET analysis with a checkpoint on the loop header
   SECTION("GENERATE_IPET_CHECKPOINT_LOOP_HEADER", "??")
   {
      // Deactivate the edges (BB2, BB3), (BB3, BB4), (BB4, BB2) and (BB2, BB5)
      deactivate_edge(main, bb2, bb3);
      deactivate_edge(main, bb3, bb4);
      deactivate_edge(main, bb4, bb2);
      deactivate_edge(main, bb2, bb5);

      // Launch the IPET analysis from BB1
      // 1 Path: BB1 -> BB2
      //   WECT   8  +   3 = 11
      long wcet = ci->launchIpetAnalysis(p, bb1);
      REQUIRE(wcet == 11);

      // Remove all attributes
      ci->RemovePrivateAttributes();
      IPET_assert_cleaned(p);

      // Deactivate the edges (BB1, BB2), (BB2, BB3) and (BB2, BB5)
      deactivate_edge(main, bb1, bb2);
      deactivate_edge(main, bb2, bb3);
      deactivate_edge(main, bb2, bb5);
      // Launch the IPET analysis from BB3
      // 1 Path:  BB3 -> BB4 -> BB2
      //   WECT    4  +   2 +   3 = 9
      wcet = ci->launchIpetAnalysis(p, bb3);
      REQUIRE(wcet == 9);
   }
}
