// BB0 2 cycles
void checkpoint() { }

// Nb cycles:
// - BB1 8
// - 10 * loop: 93
//  - Header: 3
//  - Call chkpt: 2 + 2 (j=j+i, call, nop, ret)
//  - Footer: 2
//  - Header final: 3
// - BB2 3 (nop, add, ret)
void main() {
  // BB1 8 cycles
  int i,j,k;
  j = j + k -1;
  i = 0;
  // Header: 3 Cycles
  for(; i<10; i++) {
    // Call chkpt (2 cycles)
    j = j + i;
    checkpoint();
    // Footer: 2 cycles (k++, i++)
    k++;
  }

  // BB2 3 cycles (nop, add, ret)
  return;
}
