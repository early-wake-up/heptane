#include <catch2/catch.hpp>
#include "Generic/Config.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "TestUtils.h"

TEST_CASE("TEST_IPET_SIMPLE_NO_CHKPT", "??")
{
    setup_heptane_analysis("TestIPETSimple");
    Cfg * main = config->getEntryPoint();
    Program * p = main->GetProgram();

    CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");

    long wcet = ci->launchIpetAnalysis(p, main->GetStartNode());
    REQUIRE(wcet == 15);
}

TEST_CASE("TEST_IPET_SIMPLE_CHKPT", "??")
{
    setup_heptane_analysis("TestIPETSimple");
    Cfg * main = config->getEntryPoint();
    Program * p = main->GetProgram();
    Cfg * bar = p->GetCfgByName("checkpoint");
    assert(bar != NULL && "Could not found checkpoint cfg in simple.xml");
    SerialisableIntegerAttribute attr(1);
    // Define bar as a checkpoint function
    bar->SetAttribute(CheckpointFn, attr);

    Node * bb1 = main->GetStartNode();
    // Define bb1 as a basic block calling a checkpoint function
    bb1->SetAttribute(CheckpointBB, attr);
    Node * bb2 = main->GetSuccessors(bb1)[0];
    CheckpointIsolation *ci = new CheckpointIsolation(p, "blabla");


    // Deactivate the edge between BB1 and BB2
    for(Edge * e : main->GetAllEdges()) {
       SerialisableIntegerAttribute edgeDeactivated(1);
       e->SetAttribute(EdgeDeactivated, edgeDeactivated);
    }

    // Launch the IPET analysis from BB1
    long wcet = ci->launchIpetAnalysis(p, bb1);
    REQUIRE(wcet == 10);

    ci->RemovePrivateAttributes();


    for(Edge * e : main->GetAllEdges()) {
       SerialisableIntegerAttribute edgeDeactivated(1);
       e->SetAttribute(EdgeDeactivated, edgeDeactivated);
    }

    AnalysisHelper::SetBoolAttr(main, FunctionDeactivated);
    // Launch the IPET analysis from BB2
    wcet = ci->launchIpetAnalysis(p, bb2);
    REQUIRE(wcet == 5);
}
