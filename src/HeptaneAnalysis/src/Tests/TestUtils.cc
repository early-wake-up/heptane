#include <catch2/catch.hpp>
#include <unistd.h>
#include <iostream>
#include <string>
#include <cstring> // For strerror

#include "TestUtils.h"
#include "Specific/CheckpointIsolation/CheckpointIsolation.h"
#include "Specific/IPETAnalysis/IPETAnalysis.h"
#include "Logger.h"

void setup_heptane_analysis(const std::string &relativePath)
{
   bool res = change_directory(relativePath);
   if (res == false)
   {
      std::perror("Could not change directory");
      throw std::runtime_error("Could not change directory");
   }
   // Initialisation code (do not remove, useful to create serialisation code
   // for attribute types not supported by cfglib
   AttributesFactory *af = AttributesFactory::GetInstance();
   af->SetAttributeType(AddressAttributeName, new AddressAttribute());
   af->SetAttributeType(SymbolTableAttributeName, new SymbolTableAttribute());
   af->SetAttributeType(ARMWordsAttributeName, new ARMWordsAttribute());
   af->SetAttributeType(StackInfoAttributeName, new StackInfoAttribute());
   af->SetAttributeType(CodeLineAttributeName, new CodeLineAttribute());
   af->SetAttributeType(ContextListAttributeName, new ContextList());
   af->SetAttributeType(ContextTreeAttributeName, new ContextTree());
   af->SetAttributeType(MetaInstructionAttributeName, new MetaInstructionAttribute());
   string configFile = "configWCET.xml";

   config->FillArchitectureFromXml(configFile);
   config->ExecuteFromXml(configFile, true);
   AnalysisHelper::resetCallerNodesMap();
   AnalysisHelper::resetLoopHeadersSet();
}

void IPET_assert_cleaned(Program *p)
{
   // Assert that all private attributes have been removed
   for (Cfg *cfg : p->GetAllCfgs())
   {
      for (Node *n : cfg->GetAllNodes())
      {
         REQUIRE(n->HasAttribute(InternalAttributeId) == false);
         REQUIRE(n->HasAttribute(InternalAttributeWCETfirst) == false);
         REQUIRE(n->HasAttribute(InternalAttributeWCETnext) == false);
         REQUIRE(n->HasAttribute(FrequencyAttributeName) == false);
         REQUIRE(n->HasAttribute(AnalysisStartingPointAttribute) == false);
      }
      for (Edge *e : cfg->GetAllEdges())
      {
         REQUIRE(e->HasAttribute(EdgeDeactivated) == false);
      }
   }
}

void deactivate_edge(Cfg *cfg, Node *source, Node *target)
{
   vector<Edge *> edges = cfg->GetOutgoingEdges(source);
   for (Edge *e : edges)
   {
      if (e->GetTarget() == target)
      {
         SerialisableIntegerAttribute edgeDeactivated(1);
         e->SetAttribute(EdgeDeactivated, edgeDeactivated);
         return;
      }
   }
   throw std::runtime_error("Could not find edge");
}

bool edge_is_deactivated(Cfg *cfg, Node *source, Node *target)
{
   vector<Edge *> edges = cfg->GetOutgoingEdges(source);
   for (Edge *e : edges)
   {
      if (e->GetTarget() == target)
      {
         return e->HasAttribute(EdgeDeactivated);
      }
   }
   throw std::runtime_error("Could not find edge");
}

void deactivate_function(Cfg *cfg)
{
   SerialisableIntegerAttribute funcDeactivated(1);
   cfg->SetAttribute(FunctionDeactivated, funcDeactivated);
}

std::string get_executable_folder()
{
   char buffer[PATH_MAX];
   ssize_t len = readlink("/proc/self/exe", buffer, sizeof(buffer) - 1);

   if (len != -1)
   {
      buffer[len] = '\0';
      // Remove the executable name to get the folder
      char *lastSlash = strrchr(buffer, '/');
      if (lastSlash != nullptr)
      {
         *lastSlash = '\0';
         return std::string(buffer);
      }
   }

   return ""; // Return an empty string if unable to determine the path
}

bool change_directory(const std::string &relativePath)
{
   string exec_path = get_executable_folder();
   // Construct the full path by appending the relative path
   std::string fullPath = exec_path + "/src/Tests/" + relativePath;

   // Change the current working directory
   if (chdir(fullPath.c_str()) == 0)
   {
      std::cout << "Changed directory to: " << fullPath << std::endl;
      return true;
   }
   else
   {
      std::cerr << "Error changing directory to '" << fullPath << "': " << strerror(errno) << std::endl;
      std::cerr << "Make sure that the \"tests\" executable is still where it has been generated ";
      std::cerr << "(HEPTANE_INSTALL/src/HeptaneAnalysis)" << std::endl;
      return false;
   }
}
