#ifndef TEST_UTILS_H
#define TEST_UTILS_H
#include "Generic/Config.h"

void setup_heptane_analysis(const std::string& relativePath);
void IPET_assert_cleaned(Program * p);
bool edge_is_deactivated(Cfg * cfg, Node * source, Node * target);
void deactivate_edge(Cfg * cfg, Node * source, Node * target);
void deactivate_function(Cfg * cfg);
bool change_directory(const std::string& relativePath);
#endif
