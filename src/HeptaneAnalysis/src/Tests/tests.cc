#include <catch2/catch.hpp>
#include "Generic/Config.h"

unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}


TEST_CASE( "Factorials are computed", "[factorial]" ) {
    REQUIRE( Factorial(1) == 1 );
    REQUIRE( Factorial(2) == 2 );
    REQUIRE( Factorial(3) == 6 );
    REQUIRE( Factorial(10) == 3628800 );
}

/// Create a program with a single basic block
/// and a twos instruction: SUB.W	#6, R1 and ret
TEST_CASE( "Creating program", "??") {
    Cfg *cfg = new Cfg(ListOfString({"","main"}));
    Node * bb0 = cfg->CreateNewNode(BB);
    bb0->CreateNewInstruction("sub	#6,	r1	",cfglib::Code, false);
    bb0->CreateNewInstruction("ret	", cfglib::Code, true);
    REQUIRE( bb0->GetNbInstructions() == 2 );
    REQUIRE( bb0->GetInstructions().size() == 2 );
}
